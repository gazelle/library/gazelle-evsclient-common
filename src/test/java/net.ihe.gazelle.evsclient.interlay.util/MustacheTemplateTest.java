package net.ihe.gazelle.evsclient.interlay.util;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;

public class MustacheTemplateTest {

    @Test
    public void test(){
        MustacheTemplate template = new MustacheTemplate("validation-for-creation");
        String body = template.execute(new Object() {
            String service = "serviceName";
            String validator = "validatorName";
            List<Object> objects = Arrays.asList(new Object[] {new Object() {
                String fileName = "fileName";
                String content = "contentForTest";
            }
            });
        });
        assertNotNull(body);
    }
}
