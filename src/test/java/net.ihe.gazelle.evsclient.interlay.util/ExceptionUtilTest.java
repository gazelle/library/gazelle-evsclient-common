package net.ihe.gazelle.evsclient.interlay.util;

import org.junit.Assert;
import org.junit.Test;

public class ExceptionUtilTest {

   @Test
   public void testExceptionMessageConcatenation() {
      Exception originalCause = new NullPointerException();
      Exception intermediateCause = new IllegalArgumentException("Illegal argument message.", originalCause);
      Exception exception = new IllegalStateException("Illegal state message.", intermediateCause);
      Assert.assertEquals(
            System.lineSeparator() + "Caused by: java.lang.IllegalStateException Illegal state message." +
                  System.lineSeparator() + "Caused by: java.lang.IllegalArgumentException Illegal argument message." +
                  System.lineSeparator() + "Caused by: java.lang.NullPointerException",
            ExceptionUtil.getMessageAndCauses(exception));
   }

}
