package net.ihe.gazelle.evsclient.interlay.dao;

import net.ihe.gazelle.evsclient.domain.processing.AbstractProcessing;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "evsc_processing_stub_for_test")
class ConcreteProcessingStub extends AbstractProcessing {
   private static final long serialVersionUID = 9023637333389660581L;
}
