package net.ihe.gazelle.evsclient.interlay.dao;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.NotLoadedException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingDao;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotFoundException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotSavedException;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.processing.UnexpectedProcessingException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.testng.Assert;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.Transaction;
import javax.transaction.UserTransaction;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class HandleObjectPersistanceTest {

   private static EntityManagerFactory entityManagerFactory;
   private static Set<String> testFiles ;

   @Mock
   private ApplicationPreferenceManager applicationPreferenceManager;
   private ConcreteProcessingDao dao;
   private RepositoryStub repositoryStub;

   @BeforeClass
   public static void classSetUp() {
      entityManagerFactory = Persistence.createEntityManagerFactory("PersistenceUnitTestForHandleObject");
      testFiles = new HashSet<>();
   }

   @Before
   public void testSetUp() throws SystemException, NotSupportedException, NamingException {
      repositoryStub = new RepositoryStub();
      dao = new ConcreteProcessingDao(entityManagerFactory, applicationPreferenceManager, repositoryStub);
   }

   @Test
   public void testHandleObjectFilePersistance()
         throws NotLoadedException, UnexpectedProcessingException, ProcessingNotSavedException, IOException {

      HandledObject handledObject = getHandledObject();
      ConcreteProcessingStub processing = getProcessing(handledObject);

      processing = dao.create(processing);
      dao.getCurrentTransaction().commit();

      assertTrue(processing.getObjects().get(0) instanceof HandledObjectFile);
      HandledObjectFile actualObject = (HandledObjectFile) processing.getObjects().get(0);
      assertNotNull(actualObject.getFilePath());
      assertEquals(actualObject.getOriginalFileName(), handledObject.getOriginalFileName());
      assertEquals(actualObject.getRole(), handledObject.getRole());
      assertEquals(actualObject.getContent(), handledObject.getContent());
      assertEquals(Files.readAllBytes(Paths.get(actualObject.getFilePath())), handledObject.getContent());
   }

   @Test
   public void testHandledObjectFileReload()
         throws NotLoadedException, UnexpectedProcessingException, ProcessingNotSavedException, InterruptedException {
      HandledObject handledObject = getHandledObject();
      ConcreteProcessingStub processing = getProcessing(handledObject);

      processing = dao.create(processing);
      dao.getCurrentTransaction().commit();

      dao = new ConcreteProcessingDao(entityManagerFactory, applicationPreferenceManager, repositoryStub);
      ConcreteProcessingStub processingReload = dao.getObjectByOid(processing.getOid());

      assertEquals(processingReload.getObjects().get(0).getContent(), handledObject.getContent());
   }

   @After
   public void testTearDown() {
      testFiles.addAll(dao.repositoryStub.generatedPath);
   }

   @AfterClass
   public static void classTearDown() {
      entityManagerFactory.close();
      deleteTestFiles();
   }

   private ConcreteProcessingStub getProcessing(HandledObject handledObject) {
      ConcreteProcessingStub processing = new ConcreteProcessingStub();
      processing.setOid("1.2.3.4");
      processing.setObjects(Arrays.asList(handledObject));
      return processing;
   }

   private HandledObject getHandledObject() {
      HandledObject handledObject = new HandledObject();
      handledObject.setOriginalFileName("test.xml");
      handledObject.setRole("document to validate");
      handledObject.setContent("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<test/>".getBytes(StandardCharsets.UTF_8));
      return handledObject;
   }

   private static void deleteTestFiles() {
      for(String path : testFiles) {
         try {
            Files.delete(Paths.get(path));
         } catch (IOException e) {
            e.printStackTrace();
         }
      }
   }

   static class ConcreteProcessingDao extends AbstractProcessingDaoImpl<ConcreteProcessingStub> {

      private final RepositoryStub repositoryStub;
      private EntityTransaction currentTransaction;

      public ConcreteProcessingDao(EntityManagerFactory entityManagerFactory,
                                   ApplicationPreferenceManager applicationPreferenceManager, RepositoryStub repositoryStub) {
         super(entityManagerFactory, applicationPreferenceManager);
         this.repositoryStub = repositoryStub;

      }

      @Override
      public EntityManager getEntityManager() {
         EntityManager em = super.getEntityManager();
         currentTransaction = em.getTransaction();
         currentTransaction.begin();
         return em;
      }

      public EntityTransaction getCurrentTransaction() {
         return currentTransaction;
      }

      @Override
      public Repository getRepositoryType(ConcreteProcessingStub processedObject) {
         return repositoryStub;
      }

      @Override
      protected void deleteSpecificItems(ConcreteProcessingStub processing)
            throws ProcessingNotFoundException, UnexpectedProcessingException {

      }
   }

   static class RepositoryStub implements Repository {

      private final Set<String> generatedPath = new HashSet<>();

      @Override
      public String buildFilePath(ApplicationPreferenceManager applicationPreferenceManager, String id) {
         try {
            String path = Files.createTempFile("test_", id + ".xml").toString();
            generatedPath.add(path);
            return path;
         } catch (IOException e) {
            throw new RuntimeException(e);
         }
      }

      @Override
      public String buildFilePath(ApplicationPreferenceManager applicationPreferenceManager) {
         try {
            String path = Files.createTempFile("test_", ".xml").toString();
            generatedPath.add(path);
            return path;
         } catch (IOException e) {
            throw new RuntimeException(e);
         }
      }

      @Override
      public String getRootPathPreferenceKey() {
         return "any";
      }

      public Set<String> getGeneratedPath() {
         return generatedPath;
      }
   }
}
