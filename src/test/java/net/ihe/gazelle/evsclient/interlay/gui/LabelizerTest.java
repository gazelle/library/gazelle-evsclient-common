package net.ihe.gazelle.evsclient.interlay.gui;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LabelizerTest {

    Labelizer labelizer = new Labelizer(null);

    @Test
    public void testGetCodeWithHashtagAndOther() {
        String value = "Item#!!:44";
        String code = labelizer.getCode(value);
        assertEquals("Item44", code);
    }

    @Test
    public void testGetCodeWithHyphen() {
        String value = "-,!:;Item_#44";
        String code = labelizer.getCode(value);
        assertEquals("-Item_44", code);
    }

    @Test
    public void testGetCodeWithHyphenAndDigit() {
        String value = "-3Item_#44";
        String code = labelizer.getCode(value);
        assertEquals("Item_44", code);
    }

    @Test
    public void testGetCodeWithUnderscore() {
        String value = "_;,;,Item-#44";
        String code = labelizer.getCode(value);
        assertEquals("_Item-44", code);
    }

    @Test
    public void testGetCodeWithTwoHyphens() {
        String value = "--Item-#44";
        String code = labelizer.getCode(value);
        assertEquals("Item-44", code);
    }

    @Test
    public void testGetCodeWithDigitAndHyphens() {
        String value = "9--Item-!44";
        String code = labelizer.getCode(value);
        assertEquals("--Item-44", code);
    }

    @Test
    public void testGetCodeWithHyphensAndDigit() {
        String value = "-9Item-!44";
        String code = labelizer.getCode(value);
        assertEquals("Item-44", code);
    }
}