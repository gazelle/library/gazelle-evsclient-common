package net.ihe.gazelle.evsclient.application.interfaces;

public class NotFoundException extends Exception {
   private static final long serialVersionUID = 6998045885049553555L;

   public NotFoundException() {
   }

   public NotFoundException(String s) {
      super(s);
   }

   public NotFoundException(String s, Throwable throwable) {
      super(s, throwable);
   }

   public NotFoundException(Throwable throwable) {
      super(throwable);
   }
}
