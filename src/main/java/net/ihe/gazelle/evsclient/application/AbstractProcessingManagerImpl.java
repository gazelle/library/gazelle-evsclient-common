package net.ihe.gazelle.evsclient.application;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.interfaces.ForbiddenAccessException;
import net.ihe.gazelle.evsclient.application.interfaces.NotFoundException;
import net.ihe.gazelle.evsclient.application.interfaces.UnauthorizedException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.NotLoadedException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotFoundException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotSavedException;
import net.ihe.gazelle.evsclient.application.interfaces.OidGeneratorManager;
import net.ihe.gazelle.evsclient.application.interfaces.ProcessingManager;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingDao;
import net.ihe.gazelle.evsclient.domain.processing.UnexpectedProcessingException;
import net.ihe.gazelle.evsclient.domain.processing.AbstractProcessing;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;

public abstract class AbstractProcessingManagerImpl<T extends AbstractProcessing> implements ProcessingManager<T> {

   protected ProcessingDao<T> processingDAO;

   protected ApplicationPreferenceManager applicationPreferenceManager;

   protected OidGeneratorManager oidGenerator;


   public AbstractProcessingManagerImpl(ProcessingDao<T> processingDAO,
                                        ApplicationPreferenceManager applicationPreferenceManager,
                                        OidGeneratorManager oidGenerator) {
      this.processingDAO = processingDAO;
      this.applicationPreferenceManager = applicationPreferenceManager;
      this.oidGenerator = oidGenerator;
   }

   protected T init(T selectedObject) {
      selectedObject.setOid(oidGenerator.getNewOid());
      if (selectedObject.getOwner() != null && StringUtils.isNotEmpty(selectedObject.getOwner().getUsername())) {
         selectedObject.setPrivate();
         selectedObject.setPrivacyKey(generatePrivacyKey());
      } else {
         selectedObject.setPublic();
      }
      return selectedObject;
   }

   @Override
   public void makeResultPublic(T selectedObject) {
      selectedObject.setPublic();
      processingDAO.merge(selectedObject);
   }

   @Override
   public void makeResultPrivate(T selectedObject) {
      selectedObject.setPrivate();
      processingDAO.merge(selectedObject);
   }

   /**
    * generate a privacy key made of 16 alphanumrical characters
    */
   private String generatePrivacyKey() {
      return RandomStringUtils.randomAlphanumeric(16);
   }

   @Override
   public void shareResult(T selectedObject) {
      selectedObject.setPrivacyKey(generatePrivacyKey());
      processingDAO.merge(selectedObject);
   }

   @Override
   public T merge(T selectedObject) {
      return processingDAO.merge(selectedObject);
   }

   @Override
   public T create(T selectedObject)
         throws UnexpectedProcessingException, NotLoadedException, ProcessingNotSavedException {
      return processingDAO.create(selectedObject);
   }

   @Override
   public void delete(String oid) throws ProcessingNotFoundException, UnexpectedProcessingException {
      processingDAO.delete(oid);
   }

   @Override
   public T getObjectByID(final Integer id, final String privacyKey, GazelleIdentity identity) throws NotFoundException,
         UnauthorizedException, ForbiddenAccessException {
      return authorizeRead(id.toString(), privacyKey, identity, processingDAO.getObjectById(id));
   }

   @Override
   public T getObjectByOID(String oid, final String privacyKey, GazelleIdentity identity) throws NotFoundException,
         UnauthorizedException, ForbiddenAccessException {
      return authorizeRead(oid, privacyKey, identity, processingDAO.getObjectByOid(oid));
   }

   protected T authorizeRead(String oid, String privacyKey, GazelleIdentity identity, T object)
           throws UnauthorizedException, ForbiddenAccessException, NotFoundException {
      if (object != null) {
         if (isPublic(object) || isPrivateKeyMatching(privacyKey, object)) {
            return object;
         } else if (identity.isLoggedIn()) {
            boolean isMonitorOrAdmin = identity.hasRole("monitor_role") || identity.hasRole("admin_role");
            if (isMonitorOrAdmin || isUserTheOwner(identity, object) || isUserAnAllowedMember(identity,
                  object)) {
               return object;
            } else {
               throw new ForbiddenAccessException("Forbidden access");
            }
         } else {
            throw new UnauthorizedException("Unauthorized access");
         }
      } else {
         throw new NotFoundException(String.format("Unable to find AbstractProcessing with identifier '%s'.", oid));
      }
   }

   private boolean isUserAnAllowedMember(GazelleIdentity identity, T object) {
      return applicationPreferenceManager.getBooleanValue("show_logs_from_colleagues") &&
            identity.getOrganisationKeyword().equals(object.getOwner().getOrganization());
   }

   private boolean isUserTheOwner(GazelleIdentity identity, T object) {
      return identity.getUsername().equals(object.getOwner().getUsername());
   }

   private boolean isPrivateKeyMatching(String privacyKey, T object) {
      return !StringUtils.isEmpty(privacyKey) && privacyKey.equals(object.getPrivacyKey());
   }

   private boolean isPublic(T object) {
      return object.getSharing() == null || !object.getSharing().getIsPrivate();
   }

}
