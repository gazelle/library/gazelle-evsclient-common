package net.ihe.gazelle.evsclient.application.interfaces.adapters;

import net.ihe.gazelle.evsclient.domain.OIDSequence;

public interface OidSequenceDao {

    OIDSequence merge(OIDSequence oidSequence);
}
