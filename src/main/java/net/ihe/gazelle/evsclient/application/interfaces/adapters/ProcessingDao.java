package net.ihe.gazelle.evsclient.application.interfaces.adapters;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evsclient.domain.processing.AbstractProcessing;
import net.ihe.gazelle.evsclient.domain.processing.EntryPoint;
import net.ihe.gazelle.evsclient.domain.processing.UnexpectedProcessingException;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;

import java.util.Date;
import java.util.List;

public interface ProcessingDao<T extends AbstractProcessing> {

   FilterDataModel<T> getFilteredDataModel(Filter<T> filter);

   T merge(T processing);

   T create(T processing) throws UnexpectedProcessingException, ProcessingNotSavedException, NotLoadedException;

   T getObjectById(final Integer id, final String privacyKey, boolean addSecurityRestriction, GazelleIdentity identity);

   T getObjectById(final Integer id);

   T getObjectByOid(final String oid, final String privacyKey, boolean addSecurityRestriction, GazelleIdentity identity);

   T getObjectByOid(final String oid);

   T getObjectByToolObjectIdAndToolOid(String toolObjectId, String toolOid);

   int countByEntryPoint(EntryPoint entryPoint);

   int countAll();

   List<T> findALlByDateBetween(Date startDate, Date endDate);

   void delete(String oid) throws ProcessingNotFoundException, UnexpectedProcessingException;

}
