package net.ihe.gazelle.evsclient.application.interfaces;

public interface OidGeneratorManager {

    String getNewOid();

}
