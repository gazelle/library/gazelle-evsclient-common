package net.ihe.gazelle.evsclient.application.interfaces;

public class UnauthorizedException extends Exception {
   private static final long serialVersionUID = -1805171832051784684L;

   public UnauthorizedException() {
   }

   public UnauthorizedException(String s) {
      super(s);
   }

   public UnauthorizedException(String s, Throwable throwable) {
      super(s, throwable);
   }

   public UnauthorizedException(Throwable throwable) {
      super(throwable);
   }
}
