package net.ihe.gazelle.evsclient.application;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.interfaces.OidGeneratorManager;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.OidSequenceDao;
import net.ihe.gazelle.evsclient.domain.OIDSequence;

public class OIDGeneratorManagerImpl implements OidGeneratorManager {

    private OidSequenceDao oidSequenceDAO;

    private ApplicationPreferenceManager applicationPreferenceManager;

    public OIDGeneratorManagerImpl(OidSequenceDao oidSequenceDAO, ApplicationPreferenceManager applicationPreferenceManager) {
        this.oidSequenceDAO = oidSequenceDAO;
        this.applicationPreferenceManager = applicationPreferenceManager;
    }

    @Override
    public String getNewOid() {
        OIDSequence oidSequence = new OIDSequence();
        oidSequence = oidSequenceDAO.merge(oidSequence);
        return getRootOid().concat(".").concat(Integer.toString(oidSequence.getId()));
    }

    private String getRootOid() {
        return applicationPreferenceManager.getRootOid();
    }
}
