package net.ihe.gazelle.evsclient.application.interfaces;

import net.ihe.gazelle.evsclient.application.interfaces.adapters.NotLoadedException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotFoundException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotSavedException;
import net.ihe.gazelle.evsclient.domain.processing.UnexpectedProcessingException;
import net.ihe.gazelle.evsclient.domain.processing.AbstractProcessing;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;

import java.io.IOException;

public interface ProcessingManager<T extends AbstractProcessing> {

    void makeResultPublic(T selectedObject);

    void makeResultPrivate(T selectedObject);

    void shareResult(T selectedObject);

    T merge(T selectedObject);

    T create(T selectedObject) throws UnexpectedProcessingException, NotLoadedException, ProcessingNotSavedException;

    void delete(String oid) throws ProcessingNotFoundException, IOException, UnexpectedProcessingException;

    T getObjectByID(Integer id, String privacyKey, GazelleIdentity identity) throws NotFoundException,
          UnauthorizedException, ForbiddenAccessException;

    T getObjectByOID(String oid, String privacyKey, GazelleIdentity identity) throws NotFoundException,
          UnauthorizedException, ForbiddenAccessException;

}
