package net.ihe.gazelle.evsclient.application.interfaces;

public interface ProcessingCacheManager {

    String getProcessingData(final String oid, final String externalId, final String toolOid, final String queryCache, final String objectToQuery) ;

    String getProcessingDateService(final String oid, final String externalId, final String toolOid, final String queryCache);

    String getProcessingPermanentLinkService(final String oid, final String externalId, final String toolOid, final String queryCache);

    String getProcessingStatusService(final String oid, final String externalId, final String toolOid, final String queryCache);

    boolean refreshGazelleCacheWebService(final String externalId, final String toolOid, final String oid, final String queryCache);
}
