package net.ihe.gazelle.evsclient.application.interfaces.adapters;

public class NotLoadedException extends Exception{

    public NotLoadedException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public NotLoadedException(Throwable throwable) {
        super(throwable);
    }

    public NotLoadedException(String message) {
        super(message);
    }
}
