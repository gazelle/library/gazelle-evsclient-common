package net.ihe.gazelle.evsclient.application.interfaces;

import net.ihe.gazelle.evsclient.domain.processing.AbstractProcessing;

public interface GuiPermanentLink<T extends AbstractProcessing> {

    public String getPermanentLink(T selectedObject);

}
