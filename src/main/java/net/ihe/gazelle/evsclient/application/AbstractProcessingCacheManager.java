/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.application;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.cache.GazelleCacheManager;
import net.ihe.gazelle.common.cache.GazelleCacheRequest;
import net.ihe.gazelle.common.util.DateDisplayUtil;
import net.ihe.gazelle.evsclient.application.interfaces.ProcessingCacheManager;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingDao;
import net.ihe.gazelle.evsclient.domain.processing.AbstractProcessing;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.contexts.Lifecycle;

public abstract class AbstractProcessingCacheManager<T extends AbstractProcessing> implements ProcessingCacheManager {

    private static final String VALIDATION_DATE = "EVSClient:GetValidationDate";
    private static final String VALIDATION_STATUS = "EVSClient:GetValidationStatus";
    private static final String VALIDATION_PERMANENTLINK = "EVSClient:GetValidationPermanentLink";

    private final ProcessingDao<T> processingDao;

    protected final ApplicationPreferenceManager applicationPreferenceManager;

    public AbstractProcessingCacheManager(ApplicationPreferenceManager applicationPreferenceManager, ProcessingDao<T> processingDao) {
        this.processingDao = processingDao;
        this.applicationPreferenceManager = applicationPreferenceManager;
    }

    private T getObject(final String id, final String objectType) {

        final boolean createContexts = !Contexts.isEventContextActive() && !Contexts.isApplicationContextActive();
        if (createContexts) {
            Lifecycle.beginCall();
        }
        T processing;
        if ("oid".equals(objectType)) {
            processing = processingDao.getObjectByOid(id);
        } else {
            processing = processingDao.getObjectByToolObjectIdAndToolOid(id, objectType);
        }

        if (createContexts) {
            Lifecycle.endCall();
        }
        return processing;
    }

    private String getValidationDateFromDb(final String id, final String objectType) {
        final boolean createContexts = !Contexts.isEventContextActive() && !Contexts.isApplicationContextActive();
        if (createContexts) {
            Lifecycle.beginCall();
        }
        final T processing = getObject(id, objectType);
        String result = null;
        if (processing != null) {
            result = DateDisplayUtil.displayDateTime(processing.getDate());
        }
        if (createContexts) {
            Lifecycle.endCall();
        }
        return result;
    }

    private String getValidationPermanentLinkFromDb(final String id, final String objectType) {
        final boolean createContexts = !Contexts.isEventContextActive() && !Contexts.isApplicationContextActive();
        if (createContexts) {
            Lifecycle.beginCall();
        }
        final T processing = getObject(id, objectType);
        String url = null;
        if (processing != null) {
            url = getPermanentLink(processing);
        }
        if (createContexts) {
            Lifecycle.endCall();
        }
        return url;
    }

    public abstract String getStatus(T processing);

    public abstract String getPermanentLink(T processing);

    private String getValidationStatusFromdb(final String id, final String objectType) {
        final String result;
        final boolean createContexts = !Contexts.isEventContextActive() && !Contexts.isApplicationContextActive();
        if (createContexts) {
            Lifecycle.beginCall();
        }
        final T processing = getObject(id, objectType);
        if (processing != null) {
            result = getStatus(processing);
        } else {
            result = null;
        }
        if (createContexts) {
            Lifecycle.endCall();
        }
        return result;
    }

    @Override
    public String getProcessingData(final String oid, final String externalId, final String toolOid, final String queryCache, final
    String objectToQuery) {
        String objectId;
        String objectType;
        if (oid == null && externalId == null) {
            return null;
        }
        if (oid == null) {
            objectId = externalId;
            objectType = toolOid;
        } else {
            objectId = oid;
            objectType = "oid";
        }
        final String objectTypeFinal = objectType;
        final String cachePrefix = objectTypeFinal + objectToQuery;
        switch (objectToQuery) {
            case VALIDATION_DATE:
                return GazelleCacheManager.getWithCache(new GazelleCacheRequest<String>() {
                    @Override
                    public String get(String objectId) {
                        return getValidationDateFromDb(objectId, objectTypeFinal);
                    }
                }, cachePrefix, objectId, GazelleCacheManager.CONST_1_HOURS_IN_SECONDS);
            case VALIDATION_STATUS:
                return GazelleCacheManager.getWithCache(new GazelleCacheRequest<String>() {
                    @Override
                    public String get(String objectId) {
                        return getValidationStatusFromdb(objectId, objectTypeFinal);
                    }
                }, cachePrefix, objectId, GazelleCacheManager.CONST_1_HOURS_IN_SECONDS);
            default: //"EVSClient:GetValidationPermanentLink" :
                return GazelleCacheManager.getWithCache(new GazelleCacheRequest<String>() {
                    @Override
                    public String get(String objectId) {
                        return getValidationPermanentLinkFromDb(objectId, objectTypeFinal);
                    }
                }, cachePrefix, objectId, GazelleCacheManager.CONST_1_HOURS_IN_SECONDS);


        }

    }

    @Override
    public String getProcessingDateService(final String oid, final String externalId, final String toolOid, final String queryCache) {
        return this.getProcessingData(oid, externalId, toolOid, queryCache, VALIDATION_DATE);
    }

    @Override
    public String getProcessingPermanentLinkService(final String oid, final String externalId, final String toolOid,
                                                    final String queryCache) {
        return this.getProcessingData(oid, externalId, toolOid, queryCache, VALIDATION_PERMANENTLINK);
    }

    @Override
    public String getProcessingStatusService(final String oid, final String externalId, final String toolOid, final String queryCache) {
        return this.getProcessingData(oid, externalId, toolOid, queryCache, VALIDATION_STATUS);
    }

    @Override
    public boolean refreshGazelleCacheWebService(final String externalId, final String toolOid, final String oid,
                                                 final String queryCache) {
        getProcessingDateService(oid, externalId, toolOid, queryCache);
        getProcessingStatusService(oid, externalId, toolOid, queryCache);
        getProcessingPermanentLinkService(oid, externalId, toolOid, queryCache);
        return true;
    }
}
