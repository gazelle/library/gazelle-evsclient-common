package net.ihe.gazelle.evsclient.application.interfaces.adapters;

public class ProcessingNotSavedException extends Exception{

    public ProcessingNotSavedException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public ProcessingNotSavedException(Throwable throwable) {
        super(throwable);
    }

    public ProcessingNotSavedException(String message) {
        super(message);
    }
}
