package net.ihe.gazelle.evsclient.application.interfaces.adapters;

public class ProcessingNotFoundException extends Exception{

    public ProcessingNotFoundException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public ProcessingNotFoundException(Throwable throwable) {
        super(throwable);
    }

    public ProcessingNotFoundException(String message) {
        super(message);
    }
}
