package net.ihe.gazelle.evsclient.application.interfaces;

import java.util.List;

public interface ExportMapper {

    String getDataToExport(List<Object[]> dataList, String[] headers);

}
