package net.ihe.gazelle.evsclient.interlay.gui.document;

public interface Renderer<T,O extends Renderer.Options> {
    interface Options  {
        <O> O merge(Class<O> cls, String yaml);
    }
    class RenderingException extends Exception {
        private static final long serialVersionUID = -7314802398207151593L;

        public RenderingException(Throwable throwable) {
            super(throwable);
        }
    }

    String getCode();
    String getId();
    String getStyleClass();
    String getTitle();

    Boolean isTree();

    O getOptions();
    void setOptions(O options);
    O mergeOptions(String yaml);

    T render(HandledDocument doc);
    T render(HandledDocument doc, O options);
}
