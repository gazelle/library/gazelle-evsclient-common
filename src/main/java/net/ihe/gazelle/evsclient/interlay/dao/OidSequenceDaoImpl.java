package net.ihe.gazelle.evsclient.interlay.dao;

import net.ihe.gazelle.evsclient.application.interfaces.adapters.OidSequenceDao;
import net.ihe.gazelle.evsclient.domain.OIDSequence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class OidSequenceDaoImpl implements OidSequenceDao {

    private final EntityManagerFactory entityManagerFactory;

    public OidSequenceDaoImpl(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public OIDSequence merge(OIDSequence oidSequence) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        oidSequence = entityManager.merge(oidSequence);
        entityManager.flush();
        return oidSequence;
    }
}
