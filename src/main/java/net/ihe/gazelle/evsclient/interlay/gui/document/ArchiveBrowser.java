package net.ihe.gazelle.evsclient.interlay.gui.document;

import java.util.List;

public interface ArchiveBrowser {
    List<ArchiveNode> list();
}
