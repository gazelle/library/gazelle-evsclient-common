package net.ihe.gazelle.evsclient.interlay.gui.document;

import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.common.tree.XmlNodeData;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.evsclient.interlay.gui.I18n;
import net.ihe.gazelle.evsclient.interlay.util.SyntaxDetector;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class DocumentPresenter {
    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentPresenter.class);
    private HandledDocument handledDocument;
    private List<Renderer> renderers;
    protected String activeRenderer;

    public DocumentPresenter(HandledDocument doc) {
        this.handledDocument = doc;
    }

    public boolean isDisplayableContentType() {
        return handledDocument != null
                && handledDocument.getMimeType() != null
                && (isPlainContentType()
                    || isFramableContentType()
                    || (isArchiveContentType() && (getArchiveLister() != null || getArchiveBrowser() != null)));
    }

    public boolean isPlainContentType() {
        return handledDocument == null
                || handledDocument.getMimeType() == null
                || handledDocument.getMimeType().matches(
                "(text/.*|.*(plain|xml|json))$"
        );
    }
    public boolean isFramableContentType() {
        return handledDocument == null
                || handledDocument.getMimeType() == null
                || handledDocument.getMimeType().matches(
                "(application/pdf|image/.*|)$"
        );
    }
    public boolean isArchiveContentType() {
        return handledDocument != null
                && handledDocument.getMimeType() != null
                && handledDocument.getMimeType().matches(
                ".*(zip|x-bzip2|x-rar-compressed|x-tar|x-7z-compressed|java-archive)$"
        );
    }

    public ArchiveLister getArchiveLister() {
        if (handledDocument.getMimeType() != null) {
            if (handledDocument.getMimeType().matches(".*?[/\\+](zip)$")) {
                return new ZipLister(handledDocument.getContent());
            } // else
            //// others mime types handling
            // if (handledDoc.getContent().name().matches(".*?/(tar+gzip|x-gzip)$")) {
            //     return new TgzLister(handledDoc.getContent());
            // } else ...
        }
        return null;
    }

    public ArchiveBrowser getArchiveBrowser() {
        // TODO
        return null;
    }

    public String getMainContentTabTitle() {
        return I18n.get( "net.ihe.gazelle.EVSClient-ui."+(handledDocument==null?"Empty":handledDocument.getSyntax().name())+"Content");
    }

    public boolean displayViewOptions() {
        switch (handledDocument.getSyntax()) {
            case XML:
            case JSON:
            case ER7:
            case TXT:
                return true;
            default:
                return false;
        }
    }


    public List<Renderer> getRenderers(SyntaxDetector.Syntax syntax) {
        if (renderers==null) {
            this.renderers = new ArrayList<>();
            appendRenderers(syntax);
            if (activeRenderer==null) {
                setActiveRenderer(this.renderers.get(0).getCode());
            }
        }
        return this.renderers;
    }

    private void appendRenderers(SyntaxDetector.Syntax syntax) {
        // FIXME: 11/09/2023 - replace this switch case by SPI factory injection
        switch (syntax) {
            case XML:
                this.renderers.addAll(Arrays.asList(new XmlDocumentRenderer(),new XmlTreeRenderer(), new BinDocumentRenderer()));
                EmbeddedMediaTreeRenderer medias = new EmbeddedMediaTreeRenderer();
                try {
                    GazelleTreeNodeImpl<XmlNodeData> tree = medias.toTreeNode(getDocument());
                    Iterator<Object> it = tree.getChildrenKeysIterator();
                    if (it.hasNext()) {
                        TreeNode result = tree.getChild(it.next());
                        if (!result.isLeaf()&&result.getChildrenKeysIterator().hasNext()) {
                            this.renderers.add(medias);
                        }
                    }
                } catch (Exception e) {
                }
                break;
            case HTML:
                this.renderers.addAll(Arrays.asList(new HtmlDocumentRenderer(),new BinDocumentRenderer()));
                break;
            case JSON:
                this.renderers.addAll(Arrays.asList(new JsonDocumentRenderer(),new JsonTreeRenderer(),new BinDocumentRenderer()));
                break;
            case ER7:
                this.renderers.addAll(Arrays.asList(new Er7DocumentRenderer(),new Er7TreeRenderer(),new BinDocumentRenderer()));
                break;
            case URL:
                this.renderers.addAll(Arrays.asList(new UrlDocumentRenderer()));
                break;
            case ZIP:
                this.renderers.addAll(Arrays.asList(new ZipDocumentRenderer(),new BinDocumentRenderer()));
                break;
            case BIN:
                this.renderers.addAll(Arrays.asList(new BinDocumentRenderer()));
                break;
            case PDF:
                this.renderers.addAll(Arrays.asList(new PdfDocumentRenderer(),new BinDocumentRenderer()));
                break;
            case DICOM:
                this.renderers.addAll(Arrays.asList(new DicomDocumentRenderer(), new DicomXmlDocumentRenderer(), new DicomXmlTreeRenderer(), new BinDocumentRenderer()));
                break;
            case PEM:
                this.renderers.addAll(Arrays.asList(new PemDocumentRenderer(), new BinDocumentRenderer()));
                break;
            case B64:
                this.renderers.add(new PlainDocumentRenderer());
                if (isFramableContentType()) {
                    this.renderers.add(new B64DocumentRenderer());
                } else  {
                    appendRenderers(handledDocument.getSyntax());
                }

                break;
            default:
                this.renderers.addAll(Arrays.asList(new PlainDocumentRenderer()));
        }
    }

    public List<Renderer> getRenderers() {
        return handledDocument==null?new ArrayList<Renderer>():getRenderers(handledDocument.getSyntax());
    }

    public List<Renderer> getRenderers(String ... classnames) {
        List<Renderer> renderers = new ArrayList<Renderer>();
        if (handledDocument==null) {
            return renderers;
        }
        for(String classname:classnames) {
            renderers.add(getRenderer(classname));
        }
        return renderers;
    }

    public <X extends Renderer> X getRenderer(Class<X> cls) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        return cls.getDeclaredConstructor().newInstance();
    }
    public <X extends Renderer> X getRenderer(String classname) {
        try {
            Class<X> cls = (Class<X>) Thread.currentThread().getContextClassLoader().loadClass(classname);
            return getRenderer(cls);
        } catch (Exception e) {
            LOGGER.error("net.ihe.gazelle.document.invalid-renderer-classname");
            GuiMessage.logMessage(StatusMessage.Severity.ERROR,"net.ihe.gazelle.document.invalid-renderer-classname");
            return null;
        }
    }

    public DocumentRenderer getRenderer() {
        return getRenderer(handledDocument.getSyntax());
    }

    private DocumentRenderer getRenderer(SyntaxDetector.Syntax syntax) {
        Iterator<Renderer> it = getRenderers(syntax).iterator();
        while (it.hasNext()) {
            Renderer r = it.next();
            if (r instanceof DocumentRenderer) {
                return (DocumentRenderer) r;
            }
        }
        return null;
    }

    public String render() {
        return render(getRenderer());
    }

    public String render(DocumentRenderer<?> renderer) {
        return renderer.render(handledDocument);
    }

    public HandledDocument getDocument() {
        return handledDocument;
    }

    public String getActiveRenderer() {
        return activeRenderer;
    }

    public void setActiveRenderer(String activeRenderer) {
        this.activeRenderer = activeRenderer;
    }
}
