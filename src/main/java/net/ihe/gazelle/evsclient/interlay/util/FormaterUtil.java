package net.ihe.gazelle.evsclient.interlay.util;

import net.ihe.gazelle.evsclient.interlay.factory.XmlDocumentBuilderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;

public class FormaterUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(FormaterUtil.class);

    private static final String ERROR_PERFORMING_XML_TRANSFORMATION = "Error performing XML transformation of file : ";

    private static final XmlDocumentBuilderFactory DOCUMENT_MARSHALLER_FACTORY = new XmlDocumentBuilderFactory();

    /**
     * Convert file message to pretty format
     *
     * @param xml  the string input file
     * @param indent the string indent file
     * @return String indent message
     */
    public static String loosePrettyFormat(final String xml, final int indent) {
        try {
            return prettyFormat(xml, indent);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return xml;
        }
    }

    public static String prettyFormat(String xml, int indent) throws SAXException, IOException, ParserConfigurationException, XPathExpressionException, TransformerException {
        // Turn xml string into a document
        Document document = DOCUMENT_MARSHALLER_FACTORY
                .getBuilder()
                .parse(new InputSource(new ByteArrayInputStream(xml.getBytes("utf-8"))));

        // Remove whitespaces outside tags
        document.normalize();
        XPath xPath = XPathFactory.newInstance().newXPath();
        NodeList nodeList = (NodeList) xPath.evaluate("//text()[normalize-space()='']",
                document,
                XPathConstants.NODESET);

        for (int i = 0; i < nodeList.getLength(); ++i) {
            Node node = nodeList.item(i);
            node.getParentNode().removeChild(node);
        }

        // Setup pretty print options
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setAttribute("indent-number", indent);
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        // Return pretty print xml string
        StringWriter stringWriter = new StringWriter();
        transformer.transform(new DOMSource(document), new StreamResult(stringWriter));
        return stringWriter.toString();
    }

    public static String loosePrettyFormat(final String input) {
        return loosePrettyFormat(input, 2);
    }
}
