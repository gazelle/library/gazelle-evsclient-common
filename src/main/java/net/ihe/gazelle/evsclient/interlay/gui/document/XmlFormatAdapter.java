package net.ihe.gazelle.evsclient.interlay.gui.document;

import net.ihe.gazelle.common.tree.XmlTreeDataBuilder;
import org.richfaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.*;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class XmlFormatAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(XmlFormatAdapter.class);

    protected XmlFormatAdapter() {
        super();
    }

    public static boolean isXmlWellFormed(String messageContent) {
        try {
            return assertWellFormed(messageContent);
        } catch (final IOException | ParserConfigurationException | SAXException e) {
            return false;
        }
    }

    public static boolean assertWellFormed(String messageContent) throws SAXException, ParserConfigurationException, IOException {
            final SAXParserFactory factoryBASIC = SAXParserFactory.newInstance();
            factoryBASIC.setFeature("http://xml.org/sax/features/validation", false);
            factoryBASIC.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
            factoryBASIC.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            factoryBASIC.setFeature("http://xml.org/sax/features/external-general-entities", false);
            factoryBASIC.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            factoryBASIC.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

            factoryBASIC.setValidating(false);
            factoryBASIC.setNamespaceAware(true);

            final XmlErrorHandler eh = new XmlErrorHandler();
            final SAXParser parser = factoryBASIC.newSAXParser();
            final XMLReader reader = parser.getXMLReader();
            reader.setErrorHandler(eh);

            reader.parse(
                    new InputSource(
                            new ContentConverter().toInputStream(messageContent)));

            return !eh.isContainsError();
    }

    public static TreeNode getTree(String xmlMessageContent) {
        try {
            return XmlTreeDataBuilder
                    .build(new InputSource(new ByteArrayInputStream(xmlMessageContent.getBytes(StandardCharsets.UTF_8))));
        } catch (SAXException|IOException e){
            LOGGER.error(e.getMessage());
            return null;
        }
    }
}
