package net.ihe.gazelle.evsclient.interlay.gui.document;

public class InvalidStylesheetException extends Exception {
    public InvalidStylesheetException(String xslPath) {
        super(xslPath);
    }
}
