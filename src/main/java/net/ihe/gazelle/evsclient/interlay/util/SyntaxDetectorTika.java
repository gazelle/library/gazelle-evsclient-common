package net.ihe.gazelle.evsclient.interlay.util;

import ca.uhn.hl7v2.parser.DefaultXMLParser;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.parser.XMLParser;
import ca.uhn.hl7v2.validation.impl.NoValidation;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.evsclient.interlay.gui.document.ContentConverter;
import org.apache.tika.Tika;
import org.jdom.input.SAXBuilder;

import java.io.ByteArrayInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class SyntaxDetectorTika implements SyntaxDetector {

    private static String CERTIFICATE_PATTERN="-----BEGIN CERTIFICATE-----";


    @Override
    public Syntax detectSyntax(byte[] content) {
        try {
            Tika tika = new Tika();
            String mimeType = tika.detect(content);
            if (mimeType.startsWith("text")||mimeType.matches(".*?[/\\+](xml|html|json)$")) {
                if (isURLContent(content)) {
                    return Syntax.URL;
                } else if (mimeType.matches(".*?[/\\+](er7)$")||isER7Content(content)) {
                    return Syntax.ER7;
                } else if (mimeType.matches(".*?[/\\+](xml)$")||isXMLContent(content)) {
                    return Syntax.XML;
                } else if (mimeType.matches(".*?[/\\+](json)$")||isJsonContent(content)) {
                    return Syntax.JSON;
                } else if (mimeType.matches(".*?[/\\+](html)$")) {
                    return Syntax.HTML;
                } else if (isCertificateContent(content)) {
                    return Syntax.PEM;
                } else {
                    return Syntax.TXT;
                }
            } else if (mimeType.matches(".*?[/\\+](pdf)$")) {
                return Syntax.PDF;
            } else if (mimeType.matches(".*?[/\\+](zip)$")) {
                return Syntax.ZIP;
            } else if (mimeType.matches(".*?[/\\+](dicom)$") || isDicomContent(content)) {
                return Syntax.DICOM;
            }
            return Syntax.BIN;
        } catch (Exception e) {
            return Syntax.BIN;
        }
    }


    private boolean isCertificateContent(byte[] content) {
        if (content==null||content.length<CERTIFICATE_PATTERN.length()) {
            return false;
        }
        return new String(content, StandardCharsets.US_ASCII)
                .startsWith(CERTIFICATE_PATTERN);

    }

    private boolean isDicomContent(byte[] content) {
        return ContentConverter.isDicom(content);
    }

    private boolean isURLContent(byte[] content) {
        try {
            new URL(new String(content, StandardCharsets.US_ASCII));
        } catch (MalformedURLException mue) {
            return false;
        }
        return true;
    }
    private boolean isER7Content(byte[] content) {
        try {
            PipeParser pipeParser = new PipeParser();
            pipeParser.setValidationContext(new NoValidation());
            pipeParser.parse(new String(content, StandardCharsets.UTF_8));
            return true;
        } catch (Exception e) {
            try {
                XMLParser xmlParser = new DefaultXMLParser();
                xmlParser.setValidationContext(new NoValidation());
                xmlParser.parse(new String(content, StandardCharsets.UTF_8));
                return true;
            } catch (Exception ex) {
                return false;
            }
        }
    }
    private boolean isXMLContent(byte[] content) {
        try {
            SAXBuilder sxb = new SAXBuilder();
            sxb.build(new ByteArrayInputStream(content));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isJsonContent(byte[] content) {
        try {
            new ObjectMapper().readTree(content);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
