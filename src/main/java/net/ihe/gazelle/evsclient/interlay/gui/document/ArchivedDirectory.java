package net.ihe.gazelle.evsclient.interlay.gui.document;

import java.util.List;

public interface ArchivedDirectory extends ArchiveNode {
    List<ArchivedDirectory> directories();

    List<ArchivedFile> files();
}
