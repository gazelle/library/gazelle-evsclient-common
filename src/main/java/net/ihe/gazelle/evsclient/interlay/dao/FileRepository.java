package net.ihe.gazelle.evsclient.interlay.dao;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;

public class FileRepository implements Repository {

   private final String key;

   private final String filePrefix;

   private final String fileExtension;

   public FileRepository(final String key, final String filePrefix, final String fileExtension) {
      this.key = key;
      this.filePrefix = filePrefix;
      this.fileExtension = fileExtension;
   }

   @Override
   public String buildFilePath(ApplicationPreferenceManager applicationPreferenceManager, String id) {
      final String repositoryRootPath = applicationPreferenceManager.getStringValue(getRootPathPreferenceKey());
      if (repositoryRootPath != null && !repositoryRootPath.isEmpty()) {
         return Path.build(repositoryRootPath, filePrefix, fileExtension, id);
      }
      throw new IllegalStateException(String.format("Repository %s is not defined in preferences.", getRootPathPreferenceKey()));
   }

   @Override
   public String buildFilePath(ApplicationPreferenceManager applicationPreferenceManager) {
      return buildFilePath(applicationPreferenceManager, null);
   }

   @Override
   public String getRootPathPreferenceKey() {
      return key;
   }

   public String getFilePrefix() {
      return filePrefix;
   }

}
