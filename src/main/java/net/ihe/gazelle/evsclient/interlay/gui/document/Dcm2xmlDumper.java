package net.ihe.gazelle.evsclient.interlay.gui.document;

import org.dcm4che2.tool.dcm2xml.Dcm2Xml;

import javax.xml.transform.TransformerConfigurationException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

public class Dcm2xmlDumper extends DicomDumper {
    @Override
    protected String dump(String dicomFilePath) throws TransformerConfigurationException, IOException {
        PrintStream ops = System.out;
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        try {
            Dcm2Xml dcm2xml = new Dcm2Xml();
            PrintStream nps = new PrintStream(outStream, true, StandardCharsets.UTF_8.name());
            System.setOut(nps);
            dcm2xml.setExclude(new int[]{2145386512});
            dcm2xml.convert(new File(dicomFilePath), (File) null);
            return outStream.toString(StandardCharsets.UTF_8.name());
        } finally {
            System.setOut(ops);
            outStream.close();
        }
    }

    @Override
    protected String name() {
        return "Dicom4che";
    }
}
