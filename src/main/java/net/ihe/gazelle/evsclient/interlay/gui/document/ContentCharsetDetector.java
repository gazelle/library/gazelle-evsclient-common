package net.ihe.gazelle.evsclient.interlay.gui.document;

import com.ibm.icu.text.CharsetDetector;
import org.mozilla.universalchardet.UniversalDetector;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class ContentCharsetDetector {
    public Charset detect(byte[] content) {
        try {
            BufferedInputStream bis = new BufferedInputStream(new ByteArrayInputStream(content));
            return Charset.forName(UniversalDetector.detectCharset(bis));
        } catch (IOException e) {
            return StandardCharsets.UTF_8;
        }
    }
}
