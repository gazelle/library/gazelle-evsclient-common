package net.ihe.gazelle.evsclient.interlay.gui.document;

import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.common.tree.XmlNodeData;
import net.ihe.gazelle.common.tree.XmlTreeDataBuilder;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;

public class Er7TreeRenderer extends XmlTreeRenderer {

    public Er7TreeRenderer() {
        super();
    }


    @Override
    protected GazelleTreeNodeImpl<XmlNodeData> toTreeNode(HandledDocument doc) throws RenderingException {
        if (tree==null) {
            try {
                tree = (GazelleTreeNodeImpl) XmlTreeDataBuilder
                        .build(new InputSource(new ByteArrayInputStream(filter(getXML(doc)))));
            } catch (SAXException | IOException e) {
                throw new RenderingException(e);
            }
        }
        return tree;
    }

    private String getXML(HandledDocument doc) throws RenderingException {
        return Er7FormatAdapter.getXML(toString(doc), false, null);
    }

}
