package net.ihe.gazelle.evsclient.interlay.gui.document;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.DefaultXMLParser;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.parser.XMLParser;
import ca.uhn.hl7v2.validation.impl.NoValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Er7FormatAdapter extends XmlFormatAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(Er7FormatAdapter.class);

    protected Er7FormatAdapter() {
        super();
    }

    public static String getXML(String messageContent, boolean isXmlMessage, String packageName) {

        if (isXmlMessage){
            return messageContent;
        }

        String xmlMessageContent;
        final String processedMessage = messageContent.replace("\n", "\r");
        final PipeParser pipeParser = new PipeParser();
        pipeParser.setValidationContext(new NoValidation());
        final XMLParser xmlParser = new DefaultXMLParser();
        try {
            final Message message;
            if (packageName != null) {
                message = pipeParser.parseForSpecificPackage(processedMessage, packageName);
            } else {
                message = pipeParser.parse(processedMessage);
            }

            xmlMessageContent = xmlParser.encode(message);
            return xmlMessageContent;
        } catch (HL7Exception e) {
            LOGGER.error(e.getMessage());
            return null;
        }
    }

}
