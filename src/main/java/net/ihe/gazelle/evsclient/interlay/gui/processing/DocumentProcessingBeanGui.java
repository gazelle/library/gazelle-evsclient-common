package net.ihe.gazelle.evsclient.interlay.gui.processing;

import net.ihe.gazelle.evsclient.interlay.gui.Labelizer;
import net.ihe.gazelle.evsclient.interlay.gui.document.DocumentPresenter;
import net.ihe.gazelle.evsclient.interlay.gui.document.HandledDocument;

import java.nio.charset.StandardCharsets;

public abstract class DocumentProcessingBeanGui extends ProcessingGui {


    protected static final String DEFAULT_FILE_NAME = "content.txt";

    protected boolean showMessageInGui = true;

    protected HandledDocument document;

    public DocumentPresenter getPresenter() {
        return presenter;
    }

    protected DocumentPresenter presenter;

    protected Labelizer labelizer = new Labelizer("net.ihe.gazelle.evs.");

    public DocumentProcessingBeanGui(Class<?> cls) {
        super(cls);
    }

    public boolean getShowMessageInGui() {
        return showMessageInGui;
    }

    public void setShowMessageInGui(boolean showMessageInGui) {
        this.showMessageInGui = showMessageInGui;
    }


    public void setDocument(HandledDocument document) {
        this.document = document;
        this.presenter = new DocumentPresenter(this.document);
    }
    public void setDocumentText(String message) {
        setDocumentContent(message==null?null:message.getBytes(StandardCharsets.UTF_8));
    }
    public String getDocumentText() {
        return document==null?"":document.toString();
    }

    public void setDocumentContent(byte[] content) {
        setDocument(content==null||content.length==0?null:new HandledDocument(content));
    }
    public HandledDocument getDocument() {
        return document;
    }

    public byte[] getDocumentContent() {
        return document==null?null:document.getContent();
    }


    /**
     *
     * @param messageContent This string may be use in overriding classes to compose filename
     * @return
     */
    protected String buildFileName(String messageContent) {
        return DEFAULT_FILE_NAME;
    }

    public void reset() {
        setDocument(null);
    }

    public void reset(DocumentProcessingBeanGui gui) {
        this.reset();
        if (gui!=null) {
            gui.reset();
        }
    }


}
