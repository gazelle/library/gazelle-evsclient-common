package net.ihe.gazelle.evsclient.interlay.gui.document;

import java.nio.charset.Charset;

public class PlainDocumentRenderer extends AbstractDocumentRenderer<AbstractDocumentRenderer.Options> {
    public PlainDocumentRenderer() {
        super(new AbstractDocumentRenderer.Options(true),AbstractDocumentRenderer.Options.class);
        this.options.hasPrettyViewMode = false;
        this.options.prettyView = false;
    }
    @Override
    protected String prettify(HandledDocument doc) throws RenderingException {
        throw new UnsupportedOperationException();
    }

    @Override
    protected String highlight(String message, Charset encoding) throws RenderingException {
        throw new UnsupportedOperationException();
    }

    protected String toString(HandledDocument doc) throws RenderingException {
        // do not decode B64
        return doc.toString();
    }
}
