package net.ihe.gazelle.evsclient.interlay.gui;

import com.google.common.base.CaseFormat;
import gnu.trove.map.hash.THashMap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Labelizer {

    private String prefix;
    private Map<String,String> kebabs = new THashMap<>();
    private Map<String,String> labels = new THashMap<>();


    public static String kebabCase(String value) {
        return CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_HYPHEN, value);
    }
    public static String kebabCase(Class<?> cls) {
        return kebabCase(cls.getSimpleName());
    }
    public static String snakeCase(Class<?> cls) {
        return CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, cls.getSimpleName());
    }

    public Labelizer(String prefix) {
        this.prefix = prefix;
    }


    public String getLabel(String value, Object ... params) {
        if (params!=null&&params.length>0) {
            StringBuilder sb = new StringBuilder();
            for (Object param:params) {
                sb.append('_').append(getKebab(param.toString()));
            }
            String k = getKebab(value + sb);
            if (!k.equals(value)) {
                String l = getLabel(k);
                if (!k.equals(l)) {
                    // found specialized label for key + params
                    return l;
                }
            }
        }
        String kebab = getKebab(value);
        String label = this.labels.get(kebab);
        if (label==null) {
            label = I18n.get(prefix+ kebab, params);
            this.labels.put(kebab,label);
        }
        if (label.startsWith(prefix)) {
            return value;
        }
        return label;
    }

    public String getKebab(String value) {
        String kebab = this.kebabs.get(value);
        if (kebab==null) {
            kebab = kebabCase(getCode(value));
            this.kebabs.put(value,kebab);
        }
        return kebab;
    }

    public String getCode(String value) {
        // need to sanitize because In CSS, identifiers (including element names, classes, and IDs in selectors) can contain only the characters [a-zA-Z0-9] ...
        // @see https://www.w3.org/TR/CSS2/syndata.html#characters
        return sanitizeCss2Id(StringUtils.deleteWhitespace(WordUtils.capitalize(value)));
    }


    private String sanitizeCss2Id(String code) {
        // Remove characters that do not correspond to UNICODE_CHARACTER_CLASS, digit, hyphen, or underscore
        String cleanedValue = code.replaceAll("[^\\p{L}\\p{N}\\p{Pd}_]", "");
        // Check if the cleaned string starts with a digit, two hyphens, or a hyphen followed by a digit
        if (cleanedValue.matches("^(\\d|--|-\\d).*")) {
            // If it starts with a digit, two hyphens, or a hyphen followed by a digit, remove the offending part
            cleanedValue = cleanedValue.replaceFirst("^(\\d|--|-\\d)", "");
        }
        return cleanedValue;
    }

}
