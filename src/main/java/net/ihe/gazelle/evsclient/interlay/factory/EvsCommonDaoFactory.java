package net.ihe.gazelle.evsclient.interlay.factory;

import net.ihe.gazelle.evsclient.interlay.dao.OidSequenceDaoImpl;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.OidSequenceDao;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;

import javax.persistence.EntityManagerFactory;

@Name("evsCommonDaoFactory")
@Scope(ScopeType.APPLICATION)
@AutoCreate
public class EvsCommonDaoFactory {

    @In(value = "entityManagerFactory", create = true)
    private EntityManagerFactory entityManagerFactory;

    @Factory( value = "oidSequenceDao", scope = ScopeType.APPLICATION)
    public OidSequenceDao getOidSequenceDao(){
        return new OidSequenceDaoImpl(entityManagerFactory);
    }
}
