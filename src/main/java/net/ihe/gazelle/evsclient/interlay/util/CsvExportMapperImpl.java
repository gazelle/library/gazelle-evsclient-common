package net.ihe.gazelle.evsclient.interlay.util;

import net.ihe.gazelle.evsclient.application.interfaces.ExportMapper;

import java.util.List;

public class CsvExportMapperImpl implements ExportMapper {

    @Override
    public String getDataToExport(List<Object[]> dataList, String[] headers) {
        StringBuilder csvData = new StringBuilder();
        for(String header : headers) {
            csvData.append(header).append(";");
        }
        csvData.append("\n");
        for(Object[] dataRow : dataList) {
            for(Object data : dataRow) {
                csvData.append(data).append(";");
            }
            csvData.append("\n");
        }
        return csvData.toString();
    }
}
