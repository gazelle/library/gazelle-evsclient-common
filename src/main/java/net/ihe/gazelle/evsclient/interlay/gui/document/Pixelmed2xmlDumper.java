package net.ihe.gazelle.evsclient.interlay.gui.document;

import com.pixelmed.dicom.AttributeList;
import com.pixelmed.dicom.XMLRepresentationOfDicomObjectFactory;
import org.w3c.dom.Document;

import java.io.ByteArrayOutputStream;

public class Pixelmed2xmlDumper extends DicomDumper {
    @Override
    protected String dump(String dicomFilePath) throws Exception {
        AttributeList list = new AttributeList();
        list.read(dicomFilePath);
        Document document = (new XMLRepresentationOfDicomObjectFactory()).getDocument(list);
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        XMLRepresentationOfDicomObjectFactory.write(outStream, document);
        return outStream.toString();
    }

    @Override
    protected String name() {
        return "Pixelmed";
    }
}
