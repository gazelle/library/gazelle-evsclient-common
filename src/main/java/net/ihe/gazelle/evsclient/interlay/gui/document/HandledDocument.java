package net.ihe.gazelle.evsclient.interlay.gui.document;

import ca.uhn.hl7v2.parser.DefaultXMLParser;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.parser.XMLParser;
import ca.uhn.hl7v2.validation.impl.NoValidation;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.evsclient.interlay.util.SyntaxDetector;
import net.ihe.gazelle.evsclient.interlay.util.SyntaxDetectorTika;
import org.apache.commons.codec.binary.Base64;
import org.apache.tika.Tika;
import org.jdom.input.SAXBuilder;

import java.io.ByteArrayInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class HandledDocument {
    private static String CERTIFICATE_PATTERN="-----BEGIN CERTIFICATE-----";


    private byte[] content;
    private String mimeType;
    private SyntaxDetector.Syntax syntax;
    private Charset encoding;



    public HandledDocument(byte[] content) {
        this.content = content;
        setSyntax(new SyntaxDetectorTika().detectSyntax(content));
        this.mimeType = getSyntax().getMimetype();
    }

    public byte[] getContent() {
        return content;
    }

    public SyntaxDetector.Syntax getSyntax() {
        return syntax;
    }

    public void setSyntax(SyntaxDetector.Syntax syntax) {
        this.syntax = syntax;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public Charset getEncoding() {
        if (encoding==null) {
            encoding = new ContentCharsetDetector().detect(content);
        }
        return encoding;
    }


    public byte[] toByteArray() {
        // skip Byte order mark
        return new ContentConverter().toByteArray(getContent(),getEncoding());
    }
    @Override
    public String toString() {
        // skip Byte order mark
        return new ContentConverter().toString(getContent(),getEncoding());
    }

    public String asXMLString() {
        if (SyntaxDetector.Syntax.B64.equals(this.getSyntax())) {
            return new String(Base64.decodeBase64(toString()), StandardCharsets.UTF_8);
        } else if (SyntaxDetector.Syntax.XML.equals(this.getSyntax())) {
            return toString();
        } else {
            return "<!CDATA["+toString()+"]]>";
        }
    }
    public String asJSONString() {
        if (SyntaxDetector.Syntax.B64.equals(this.getSyntax())) {
            return new String(Base64.decodeBase64(toString()), StandardCharsets.UTF_8);
        } else if (SyntaxDetector.Syntax.JSON.equals(this.getSyntax())) {
            return toString();
        } else {
            return "";
        }
    }
}
