package net.ihe.gazelle.evsclient.interlay.gui.document;

import javax.faces.model.SelectItem;
import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.util.List;

public class StyledDocumentRenderer extends AbstractDocumentRenderer<AbstractDocumentRenderer.Options> {
    private String style;

    private List<SelectItem> renderingModes;

    public StyledDocumentRenderer(String style) {
        super(new AbstractDocumentRenderer.Options(true),AbstractDocumentRenderer.Options.class);
        this.options.hasPrettyViewMode = false;
        this.options.hasViewLineNumberMode = false;
        this.options.prettyView = true;
        this.options.viewLineNumber = false;
        this.style = style;
        //this.options.hasRenderingModes=true;

    }

    @Override
    public String render(HandledDocument doc) {
        if (doc == null) {
            return emptyRendering();
        }
        return ReportConverter.transformXMLToHTML(
                new ByteArrayInputStream(doc.getContent()),
                style);
    }

    @Override
    protected String prettify(HandledDocument doc) throws RenderingException {
        throw new UnsupportedOperationException();
    }

    @Override
    protected String highlight(String message, Charset encoding) throws RenderingException {
        throw new UnsupportedOperationException();
    }

    public List<SelectItem> getRenderingModes() {
        return renderingModes;
    }

    public void setRenderingModes(List<SelectItem> renderingModes) {
        this.renderingModes = renderingModes;
    }
}
