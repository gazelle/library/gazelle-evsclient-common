package net.ihe.gazelle.evsclient.interlay.gui.document;

import com.uwyn.jhighlight.renderer.XhtmlRendererFactory;
import net.ihe.gazelle.evsclient.interlay.gui.I18n;
import net.ihe.gazelle.evsclient.interlay.util.FormaterUtil;
import org.apache.commons.lang.StringUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jfree.util.Log;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XmlDocumentRenderer extends AbstractDocumentRenderer<AbstractDocumentRenderer.Options> {
    public XmlDocumentRenderer() {
        super(new AbstractDocumentRenderer.Options(true),AbstractDocumentRenderer.Options.class);
    }

    @Override
    public String render(HandledDocument doc) {
        if (doc == null) {
            return emptyRendering();
        }

        Map<Integer, String> listErrors = new HashMap<>();
        Map<Integer, String> listWarnings = new HashMap<>();
        try {
            extractListXMLErrorsAndWarnings(
                    toString(doc),
                    listErrors,
                    listWarnings);
        } catch (Exception e) {
            Log.warn(I18n.get("net.ihe.gazelle.document.unable-to-render", doc.getSyntax(), e.getMessage()), e);
        }
        return render(doc, listErrors, listWarnings);
    }

    private void extractListXMLErrorsAndWarnings(final String validationResult, final Map<Integer, String> listXMLErrors,
                                                 final Map<Integer, String> listXMLWarnings) {
        if (validationResult != null && listXMLErrors != null) {
            try {
                final SAXBuilder sxb = new SAXBuilder();

                final Document dom = sxb.build(new ByteArrayInputStream(validationResult.getBytes(StandardCharsets.UTF_8)));

                final Element rac = dom.getRootElement();
                if (rac != null) {
                    Element xsd = rac.getChild("DocumentValidXSD");
                    if (xsd == null) {
                        xsd = rac.getChild("DocumentWellFormed");
                    }
                    if (xsd != null) {
                        final List<Element> mess = xsd.getChildren("XSDMessage");
                        if (mess != null && (!mess.isEmpty())) {
                            for (final Element mm : mess) {
                                if (StringUtils.isNumeric(mm.getChildText("lineNumber"))) {
                                    if ("warning".equalsIgnoreCase(mm.getChildText("Severity"))) {
                                        listXMLWarnings.put(
                                                Integer.valueOf(mm.getChildText("lineNumber")),
                                                String.valueOf(mm.getChildText("Message")));
                                    }
                                    if ("error".equalsIgnoreCase(mm.getChildText("Severity"))) {
                                        listXMLErrors.put(
                                                Integer.valueOf(mm.getChildText("lineNumber")),
                                                String.valueOf(mm.getChildText("Message")));
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (JDOMException | IOException e) {}
        }
    }


    @Override
    protected String prettify(HandledDocument doc) throws RenderingException {
        try {
            return FormaterUtil.prettyFormat(toString(doc),2);
        } catch (Exception e) {
            throw new RenderingException(e);
        }
    }

    @Override
    protected String highlight(String message, Charset encoding) throws RenderingException {
        try {
            return XhtmlRendererFactory.getRenderer("xml")
                    .highlight("xml", message, encoding.name(), true);
        } catch (IOException e) {
            throw new RenderingException(e);
        }
    }
}
