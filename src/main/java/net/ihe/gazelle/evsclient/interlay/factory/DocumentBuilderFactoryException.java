package net.ihe.gazelle.evsclient.interlay.factory;

public class DocumentBuilderFactoryException extends RuntimeException {


    private static final long serialVersionUID = 789302987642069015L;

    public DocumentBuilderFactoryException() {
        super();
    }

    public DocumentBuilderFactoryException(String s) {
        super(s);
    }

    public DocumentBuilderFactoryException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public DocumentBuilderFactoryException(Throwable throwable) {
        super(throwable);
    }
}
