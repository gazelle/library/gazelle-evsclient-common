package net.ihe.gazelle.evsclient.interlay.gui.document;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import net.ihe.gazelle.evsclient.interlay.gui.Labelizer;
import net.ihe.gazelle.evsclient.interlay.util.SyntaxDetector;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public abstract class AbstractRenderer<T,O extends AbstractRenderer.Options> implements Renderer<T,O> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRenderer.class);

    public static class Options implements Renderer.Options {

        protected String caption;
        protected Boolean hasRenderingModes;
        protected String renderingMode;

        public Options(boolean initDefaultValues) {
            super();
            if (initDefaultValues) {
                caption = "";
                hasRenderingModes = false;
                renderingMode = null;
            }
        }

        public Options() {
            this(false);
        }
        public String getCaption() {
            return caption;
        }

        public void setCaption(String caption) {
            this.caption = caption;
        }

        public Boolean hasRenderingModes() {
            return hasRenderingModes;
        }

        public String getRenderingMode() {
            return renderingMode;
        }

        public void setRenderingMode(String renderingMode) {
            this.renderingMode = renderingMode;
        }

        protected <X> X merge(X options) {
            AbstractRenderer.Options opt = (AbstractRenderer.Options) options;
            opt.caption = opt.getCaption()==null?this.caption:opt.getCaption();
            opt.hasRenderingModes = opt.hasRenderingModes==null?this.hasRenderingModes:opt.hasRenderingModes;
            opt.renderingMode = opt.renderingMode ==null?this.renderingMode :opt.renderingMode;
            return (X)opt;
        }
        @Override
        public <X> X merge(Class<X> cls, String yaml) {
            if (yaml!=null) {
                try {
                    ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
                    return merge(mapper.readValue(yaml,cls));
                } catch (IOException e) {
                    LOGGER.warn("net.ihe.gazelle.document.invalid-renderer-options",e);
                }
            }
            return (X)this;
        }
    }

    protected O options;
    protected Class<O> optionsClass;
    protected String originalCaption;
    protected String warningCaption;
    protected Labelizer labelizer;

    protected AbstractRenderer(O options, Class<O> optionsClass) {
        this.options = options;
        this.optionsClass = optionsClass;
        this.labelizer = new Labelizer("net.ihe.gazelle.document.");
    }

    @Override
    public O getOptions() {
        return options;
    }

    public void setOptions(O options) {
        this.options = options;
    }

    @Override
    public T render(HandledDocument doc, O options) {
        O defaults = this.options; // save options
        if (options != null) {
            this.options = options;
        }
        if (this.warningCaption!=null) {
            if (this.warningCaption.equals(this.options.caption)) {
                this.options.caption = this.originalCaption==null?"":this.originalCaption;
            }
            this.warningCaption = null;
        }
        try {
            if (doc == null) {
                return emptyRendering();
            }
            return render(doc);
        } finally {
            this.options = defaults; // restore options
        }
    }

    public O mergeOptions(String yaml) {
        return this.options.merge(this.optionsClass,yaml);
    }

    protected abstract T emptyRendering();

    public String getId() {
        return this.getClass().getSimpleName()+"_"+Integer.toHexString(this.hashCode());
    }
    public String getCode() {
        return this.getClass().getSimpleName();
    }

    public String getStyleClass() {
        return this.getClass().getSimpleName();
    }

    public String getTitle() {
        return getLabel(this.getClass().getSimpleName());
    }
    public String getLabel(String key, Object ... params) {
        return labelizer.getLabel(key,params);
    }
    public String getLabel(String value) {
        return labelizer.getLabel(value);
    }
    public String getWarningCaption() {
        return this.warningCaption;
    }

    protected String toString(HandledDocument doc) throws RenderingException {
        return SyntaxDetector.Syntax.B64.equals(doc.getSyntax())
                ? new String(Base64.decodeBase64(toByteArray(doc)), StandardCharsets.UTF_8)
                : doc.toString();
    }

    protected byte[] toByteArray(HandledDocument doc) throws RenderingException {
        return SyntaxDetector.Syntax.B64.equals(doc.getSyntax())
                ? Base64.decodeBase64(doc.getContent())
                : doc.getContent();
    }
}
