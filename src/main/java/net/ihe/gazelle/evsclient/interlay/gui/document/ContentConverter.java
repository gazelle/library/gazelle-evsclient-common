package net.ihe.gazelle.evsclient.interlay.gui.document;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BOMInputStream;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class ContentConverter {
    private static byte[] DICOM_BYTE_PATTERN;
    static {
        String pattern = "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004449434D";
        int len = pattern.length();
        DICOM_BYTE_PATTERN = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            DICOM_BYTE_PATTERN[i / 2] = (byte) ((Character.digit(pattern.charAt(i), 16) << 4)
                    + Character.digit(pattern.charAt(i+1), 16));
        }
    }
    public static boolean isDicom(byte[] content) {
        if (content==null||content.length<DICOM_BYTE_PATTERN.length) {
            return false;
        };
        for (int i=0 ; i<DICOM_BYTE_PATTERN.length ; i++) {
            if (content[i]!=DICOM_BYTE_PATTERN[i]) {
                return false;
            }
        }
        return true;
    }

    public static boolean isPdf(byte[] content) { // TODO is it true ?
        return content!=null
                && content.length>=4
                && content[0]=='%'
                && content[1]=='P'
                && content[2]=='D'
                && content[3]=='F';
    }

    public static boolean isZip(byte[] content) {
        return content!=null
                && content.length>=4
                && content[0]=='P'
                && content[1]=='K'
                && content[2]==0x03
                && content[3]==0x04;
    }
    public String toString(byte[] content) {
        return toString(content,StandardCharsets.UTF_8);
    }
    public String toString(byte[] content, Charset charset) {
        return new String(toByteArray(content,charset),charset);
    }
    public byte[] toByteArray(byte[] content, Charset charset) {

        if (isBinaryContent(content)) {
            return content;
        }
        // skip Byte order mark
        try {
            return IOUtils.toByteArray(toInputStreamReader(content,charset));
        } catch (IOException e) {
            return null;
        }
    }

    private boolean isBinaryContent(byte[] content) {
        return isZip(content) || isDicom(content) || isPdf(content);
    }

    public byte[] toByteArray(byte[] content, Charset charset, int from, int to) {
        byte[] fragment = new byte[to-from];
        byte[] source = toByteArray(content, charset);
        if (source==null) {
            return null;
        }
        System.arraycopy(source, from, fragment, 0, fragment.length);
        return fragment;
    }
    public byte[] toByteArray(byte[] content, int from, int to) {
        return toByteArray(content,StandardCharsets.UTF_8,from,to);
    }
    public byte[] toByteArray(String str, Charset charset, int from, int to) {
        return toByteArray(str.getBytes(StandardCharsets.UTF_8),charset, from,to);
    }
    public byte[] toByteArray(String str, int from, int to) {
        return toByteArray(str.getBytes(StandardCharsets.UTF_8),StandardCharsets.UTF_8, from,to);
    }
    public String strByOffsets(String str, Charset charset, int from, int to) {
        return new String(toByteArray(str,charset, from,to),charset);
    }
    public String strByOffsets(String str, int from, int to) {
        return strByOffsets(str, StandardCharsets.UTF_8, from, to);
    }
    public String strByOffsets(byte[] bytes, int from, int to) {
        return new String(toByteArray(bytes, StandardCharsets.UTF_8, from, to));
    }
    public InputStreamReader toInputStreamReader(byte[] content, Charset charset) {
        // skip Byte order mark
        return new InputStreamReader(
                toInputStream(content),
                charset);
    }
    public InputStream toInputStream(byte[] content) {
        // skip Byte order mark
        return new BOMInputStream(new ByteArrayInputStream(content));
    }
    public InputStream toInputStream(String content) {
        // skip Byte order mark
        return toInputStream(content.getBytes(StandardCharsets.UTF_8));
    }
    public byte[] protect(byte[] content) {
        if (isBinaryContent(content)) {
            return content;
        }
        return convert(content,StandardCharsets.UTF_8,StandardCharsets.UTF_8);
    }
    public byte[] convert(byte[] content, Charset from, Charset to) {
        if (from==to) {
            return toByteArray(content,from);
        } else {
            return toString(content, from).getBytes(to);
        }
    }
    public byte[] protect(String content) {
        return convert(content.getBytes(StandardCharsets.UTF_8),StandardCharsets.UTF_8,StandardCharsets.UTF_8);
    }

}
