package net.ihe.gazelle.evsclient.interlay.gui.document;

import java.io.File;
import java.io.FileOutputStream;

public abstract class DicomDumper {

    public DicomDumper() {
        super();
    }

    public String dump(byte[] bytes) throws Renderer.RenderingException {
        try {
            File dicomFile = File.createTempFile( "dicom_dumper_", ".dicom");
            FileOutputStream outputStream = new FileOutputStream(dicomFile);
            try {
                outputStream.write(bytes);
            } finally {
                outputStream.close();
            }
            return dump(dicomFile.getAbsolutePath());
        } catch (Exception ioe) {
            throw new Renderer.RenderingException(ioe);
        }
    }

    protected abstract String dump(String dicomFilePath) throws Exception;

    protected abstract String name();

    protected boolean isAvailable() {
        return true;
    }

}
