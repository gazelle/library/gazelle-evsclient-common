package net.ihe.gazelle.evsclient.interlay.gui.document;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.evsclient.interlay.gui.I18n;
import net.ihe.gazelle.evsclient.interlay.util.SyntaxDetector;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.jfree.util.Log;

import java.nio.charset.Charset;
import java.util.Map;

public abstract class AbstractDocumentRenderer<O extends AbstractDocumentRenderer.Options> extends AbstractRenderer<String,O> implements DocumentRenderer<O> {

    public static class Options extends AbstractRenderer.Options implements DocumentRenderer.Options {
        protected Boolean hasViewLineNumberMode;
        protected Boolean hasPrettyViewMode;
        protected Boolean viewLineNumber;
        protected Boolean hasMaxColumnsMode;
        protected Boolean prettyView;
        protected Integer maxColumns;

        public Options(boolean initDefaultValues) {
            super(initDefaultValues);
            if (initDefaultValues) {
                hasViewLineNumberMode = true;
                hasPrettyViewMode = true;
                viewLineNumber = true;
                hasMaxColumnsMode = false;
                prettyView = true;
                maxColumns = 80;
            }
        }
        public Options() {
            this(false);
        }

        public String getMaxColumns() {
            return Integer.toString(maxColumns);
        }

        public void setMaxColumns(String max) {
            this.maxColumns = Integer.parseInt(max);
        }

        @Override
        public Boolean hasMaxColumnsMode() {
            return hasMaxColumnsMode;
        }

        public Boolean getViewLineNumber() {
            return viewLineNumber;
        }

        public void setViewLineNumber(Boolean viewLineNumber) {
            this.viewLineNumber = viewLineNumber;
        }

        public Boolean getPrettyView() {
            return prettyView;
        }

        public void setPrettyView(Boolean prettyView) {
            this.prettyView = prettyView;
        }

        public Boolean hasViewLineNumberMode() {
            return hasViewLineNumberMode;
        }

        public Boolean hasPrettyViewMode() {
            return hasPrettyViewMode;
        }

        protected <X> X merge(X options) {
            AbstractDocumentRenderer.Options opt = super.merge((AbstractDocumentRenderer.Options) options);;
            opt.hasViewLineNumberMode = opt.hasViewLineNumberMode()==null?this.hasViewLineNumberMode:opt.hasViewLineNumberMode();
            opt.hasPrettyViewMode = opt.hasPrettyViewMode()==null?this.hasPrettyViewMode:opt.hasPrettyViewMode();
            opt.viewLineNumber = opt.getViewLineNumber()==null?this.viewLineNumber:opt.getViewLineNumber();
            opt.prettyView = opt.getPrettyView()==null?this.prettyView:opt.getPrettyView();
            opt.hasMaxColumnsMode = opt.hasMaxColumnsMode==null?this.hasMaxColumnsMode:opt.hasMaxColumnsMode;
            opt.maxColumns = opt.maxColumns==null?this.maxColumns:opt.maxColumns;
            return (X)opt;
        }
    }

    protected AbstractDocumentRenderer(O options,Class<O> optionsClass) {
        super(options,optionsClass);
        try {
            this.options.prettyView = ApplicationPreferenceManagerImpl.instance().getBooleanValue("document_pretty_view_mode");
        } catch (Exception e) {
            this.options.prettyView = true;
        }
    }

    public String render(HandledDocument doc,
                         final Map<Integer, String> listErrors,
                         final Map<Integer, String> listWarnings) {
        try {
            String message;
            if (options.prettyView) {
                try {
                    message = highlight(
                            prettify(doc),
                            doc.getEncoding());
                } catch (Exception e) {
                    this.originalCaption = this.warningCaption==null?options.caption:this.originalCaption;
                    String warningMessage = I18n.get("net.ihe.gazelle.document.unable-to-prettify", doc.getSyntax(), e.getMessage());
                    options.caption="<span class=\"rendering-error\">"+ StringEscapeUtils.escapeHtml(warningMessage) +"</span>";
                    this.warningCaption = options.caption;
                    message = StringEscapeUtils.escapeHtml(toString(doc));
                }
            } else {
                message = StringEscapeUtils.escapeHtml(toString(doc));
            }
            return render(
                    message.split("\\n"),
                    doc.getSyntax(),
                    listErrors,
                    listWarnings);
        } catch (Exception e) {
            String warningMessage = I18n.get("net.ihe.gazelle.document.unable-to-render", doc.getSyntax(), e.getMessage());
            Log.warn(warningMessage, e);
            return warningMessage;
        }

    }


    public String render(String[] listline,
                         SyntaxDetector.Syntax syntax,
                         final Map<Integer, String> listErrors,
                         final Map<Integer, String> listWarnings) {
            final StringBuilder html = new StringBuilder();
            if (options.prettyView) {
                html.append("<table class=\""+syntax+"-wrap prettified-bg\">\n");
                if (StringUtils.isNotEmpty(options.caption)) {
                    html.append("<caption style=\"width: 100%;\">")
                            .append(options.caption)
                            .append("</caption>\n");
                }
                html.append("<tr>\n");
            }
            else {
                html.append("<table class=\""+syntax+"-wrap\">\n<tr>\n");
            }

            Integer i = 0;
            int line = 1;
            for (final String string : listline) {
                if (string.matches("<!-- .*?generated by JHighlight.*-->")) {
                    continue;
                }
                if (this.options.viewLineNumber) {
                    html.append("<td class=\"idclass\"><a name=\"line_").append(line).append("\">").append(line)
                            .append("</a></td>\n");
                } else {
                    html.append("<td class=\"idclass\" style=\"display:none;\"><a name=\"line_").append(line).append("\">")
                            .append(line).append("</a></td>\n");
                }
                if (listErrors != null && listErrors.containsKey(i)) {
                    html.append("<td><span class=\""+syntax+"-bad\" title=\"").append(StringEscapeUtils.escapeHtml(listErrors.get(i))).append("\">").append(string).append("</span></td>\n");
                } else if (listWarnings != null && listWarnings.containsKey(i)) {
                    html.append("<td><span class=\""+syntax+"-warning\" title=\"").append(StringEscapeUtils.escapeHtml(listWarnings.get(i))).append("\">").append(string).append("</span></td>\n");
                } else {
                    html.append("<td>").append(string).append("</td>\n");
                }
                html.append("</tr>\n");
                html.append("<tr>\n");
                i++;
                line++;
            }
            html.append("</tr>\n</table>\n");
            return html.toString();

    }

    public String render(HandledDocument doc) {
        if (doc == null) {
            return emptyRendering();
        }
        return render(doc,null,null);
    }

    @Override
    protected String emptyRendering() {
        return "";
    }

    protected abstract String prettify(HandledDocument doc) throws RenderingException;
    protected abstract String highlight(String message, Charset encoding) throws RenderingException;

    @Override
    public Boolean isTree() {
        return false;
    }


}
