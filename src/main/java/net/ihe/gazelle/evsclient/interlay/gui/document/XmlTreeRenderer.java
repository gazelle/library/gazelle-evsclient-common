package net.ihe.gazelle.evsclient.interlay.gui.document;

import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.uwyn.jhighlight.renderer.XhtmlRendererFactory;
import gnu.trove.map.hash.THashMap;
import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.common.tree.XmlNodeData;
import net.ihe.gazelle.evsclient.interlay.factory.DocumentBuilderFactoryException;
import net.ihe.gazelle.evsclient.interlay.factory.XmlDocumentBuilderFactory;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.digester.Digester;
import org.apache.commons.digester.RulesBase;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.model.TreeNode;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class XmlTreeRenderer extends AbstractTreeRenderer<GazelleTreeNodeImpl<XmlNodeData>,XmlTreeRenderer.Options> {

    public static class Options extends AbstractTreeRenderer.Options {
        protected String root;

        public Options(boolean initDefaultValues) {
            super(initDefaultValues);
            if (initDefaultValues) {
                this.root = "result";
            }
        }
        public Options() {
            this(false);
        }

        public String getRoot() {
            return root;
        }

        @Override
        protected <X> X merge(X options) {
            XmlTreeRenderer.Options opt = super.merge((XmlTreeRenderer.Options) options);
            opt.root = opt.root==null?this.root:opt.root;
            return (X)opt;
        }
    }

    public static TreeNode build(InputSource inputSource) throws SAXException, IOException, ParserConfigurationException {
        Digester digester = new Digester();
        XmlTreeRenderer.Rule rule = new XmlTreeRenderer.Rule();
        final List rulesList = new ArrayList(1);
        rulesList.add(rule);
        RulesBase rulesBase = new RulesBase() {
            protected List lookup(String namespace, String name) {
                return rulesList;
            }
        };

        digester.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        digester.setRules(rulesBase);
        digester.setNamespaceAware(true);
        digester.parse(inputSource);

        return rule.treeNode;
    }

    private static final class Rule extends org.apache.commons.digester.Rule {
        private int level;
        private List idsList;
        private List treeNodesList;
        private List exclusionSets;
        private GazelleTreeNodeImpl treeNode;

        private Rule() {
            this.level = -1;
            this.idsList = new ArrayList();
            this.treeNodesList = new ArrayList();
            this.exclusionSets = new ArrayList();
            this.treeNode = new GazelleTreeNodeImpl();
        }

        public void begin(String namespace, String name, Attributes attributes) throws Exception {
            super.begin(namespace, name, attributes);
            ++this.level;
            XmlNodeData xmlNodeData = new XmlNodeData();
            xmlNodeData.setName(name);
            xmlNodeData.setNamespace(namespace);
            String id = null;
            int currentId;
            int i;
            if (attributes != null) {
                currentId = attributes.getLength();

                for(i = 0; i < currentId; ++i) {
                    xmlNodeData.setAttribute(attributes.getQName(i), attributes.getValue(i));
                }

                id = attributes.getValue("id");
            }

            if (this.exclusionSets.size() == this.level) {
                this.exclusionSets.add((Object)null);
            }

            if (id != null && id.length() != 0) {
                Set exclusions = (Set)this.exclusionSets.get(this.level);
                if (exclusions == null) {
                    exclusions = new HashSet();
                    this.exclusionSets.set(this.level, exclusions);
                }

                ((Set)exclusions).add(id);
            } else {
                currentId = 0;
                if (this.idsList.size() <= this.level) {
                    for(i = this.idsList.size(); i <= this.level; ++i) {
                        this.idsList.add((Object)null);
                    }
                } else {
                    Integer integer = (Integer)this.idsList.get(this.level);
                    currentId = integer + 1;
                }

                for(Set exclusions = (Set)this.exclusionSets.get(this.level); exclusions != null && exclusions.contains(Integer.toString(currentId)); ++currentId) {
                }

                this.idsList.set(this.level, new Integer(currentId));
                id = Integer.toString(currentId);
            }

            GazelleTreeNodeImpl node = new GazelleTreeNodeImpl();
            node.setData(xmlNodeData);
            this.treeNode.addChild(id, node);
            this.treeNodesList.add(this.treeNode);
            this.treeNode = node;
        }

        public void body(String namespace, String name, String text) throws Exception {
            super.body(namespace, name, text);
            if (text != null) {
                ((XmlNodeData)this.treeNode.getData()).setText(text.trim());
            }

        }

        public void end(String namespace, String name) throws Exception {
            super.end(namespace, name);
            --this.level;
            if (this.idsList.size() - 1 > this.level + 1) {
                this.idsList.remove(this.idsList.size() - 1);
            }

            if (this.exclusionSets.size() - 1 > this.level + 1) {
                this.exclusionSets.remove(this.exclusionSets.size() - 1);
            }

            this.treeNode = (GazelleTreeNodeImpl)this.treeNodesList.remove(this.treeNodesList.size() - 1);
        }
    }

    protected Map<Long, Integer> media;
    protected List<File> downloads;
    protected com.uwyn.jhighlight.renderer.Renderer highlighter = XhtmlRendererFactory.getRenderer("xml");
    protected HashFunction hashFunction = Hashing.md5();

    public XmlTreeRenderer() {
        super(new Options(true),Options.class);
        media = new THashMap<>();
        downloads = new ArrayList<>();
    }

    @Override
    public Boolean isActionAppropriate(String action, GazelleTreeNodeImpl<XmlNodeData> node) {
        switch (action) {
            case "download":
            case "show": return isDownloadableNode(node);
            case "validation": return isValidatedNode(node);
            case "validate": return isValidableNode(node);
        }
        return null;
    }

    @Override
    protected GazelleTreeNodeImpl<XmlNodeData> toTreeNode(HandledDocument doc) throws RenderingException {
        if (tree==null) {
            try {
                tree = (GazelleTreeNodeImpl) XmlTreeRenderer
                        .build(new InputSource(new ByteArrayInputStream(filter(doc))));
            } catch (SAXException | IOException | ParserConfigurationException e) {
                throw new RenderingException(e);
            }
        }
        return tree;
    }

    private void clean(Node node) {
        NodeList childs = node.getChildNodes();
        for (int n = childs.getLength() - 1; n >= 0; n--) {
            Node child = childs.item(n);
            short nodeType = child.getNodeType();
            if (nodeType == Node.ELEMENT_NODE) {
                clean(child);
            }
            else if (nodeType == Node.TEXT_NODE) {
                String trimmedNodeVal = child.getNodeValue().trim();
                if (trimmedNodeVal.length() == 0){
                    node.removeChild(child);
                }
                else {
                    child.setNodeValue(trimmedNodeVal);
                }
            } else if (nodeType == Node.COMMENT_NODE) {
                node.removeChild(child);
            }
        }
    }

    @Override
    protected GazelleTreeNodeImpl<XmlNodeData> emptyRendering() {
        return new GazelleTreeNodeImpl<>();
    }

    protected byte[] filter(HandledDocument doc) {
        return filter(doc.asXMLString());
    }
    protected byte[] filter(String xmldoc) {
        if (this.nodeFilter==null) {
            return xmldoc.getBytes(StandardCharsets.UTF_8);
        } else {
            try {
                DocumentBuilder builder = new XmlDocumentBuilderFactory().getBuilder();
                Document xmlDocument = builder
                        .parse(new ByteArrayInputStream(xmldoc.getBytes(StandardCharsets.UTF_8)));
                XPath xPath = XPathFactory.newInstance().newXPath();
                String expression = this.nodeFilter;
                NodeList nodeList = (NodeList) xPath.compile(expression)
                        .evaluate(xmlDocument, XPathConstants.NODESET);

                Document xml = builder.newDocument();
                Element root = xml.createElement(this.options.root);
                xml.appendChild(root);
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node node = nodeList.item(i);
                    Node copyNode = xml.importNode(node, true);

                    root.appendChild(copyNode);
                }
                DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
                DOMImplementationLS domImplLS = (DOMImplementationLS) registry.getDOMImplementation("LS");

                LSSerializer ser = domImplLS.createLSSerializer();

                LSOutput out = domImplLS.createLSOutput();
                StringWriter stringOut = new StringWriter();
                out.setCharacterStream(stringOut);
                ser.write(xml, out);

                return stringOut.toString().getBytes(StandardCharsets.UTF_8);
            } catch (IOException | DocumentBuilderFactoryException | SAXException | XPathExpressionException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
                return new byte[]{};
            }
        }
    }


    @Override
    public String renderNode(GazelleTreeNodeImpl<XmlNodeData> node) {
        String xml = serialize(node);
        if (isDownloadableNode(node)&&!media.containsKey(id(node))) {
            media.put(id(node),downloads.size());
            downloads.add(null);
        }
        try {
            if (xml.contains("<")) {
                return highlighter.highlight("xml", xml, StandardCharsets.UTF_8.name(), true);
            }
        } catch (IOException e) {
        }
        return "<span style=\"color\">"+xml+"</span>";
    }

    protected String serialize(GazelleTreeNodeImpl<XmlNodeData> node) {
        StringBuilder sb = new StringBuilder();
        XmlNodeData data = node.getData();
        if (node.getData()!=null) {
            sb.append("<");
            if (StringUtils.isNotEmpty(data.getNamespace())) {
                sb.append(data.getNamespace()).append(":");
            }
            sb.append(data.getName());
            if (!MapUtils.isEmpty(data.getAttributes())) {
                for (Map.Entry<String, Object> e: data.getAttributes().entrySet()) {
                    sb.append(" ")
                        .append(e.getKey())
                        .append("=\"")
                        .append(e.getValue()==null?"":e.getValue().toString())
                        .append("\"");
                }
            }
            sb.append(">");
            if (StringUtils.isNotEmpty(data.getText())) {
                sb.append(data.getText());
            }
        }
        return sb.toString();
    }

    public void nodeToFile(GazelleTreeNodeImpl<XmlNodeData> node, boolean download) throws IOException {
        if (media.containsKey(id(node))) {
            String filename = downloadFilename(node);
            File f = getFile(node);
            new B64Exporter().showOrDownload(download, filename, f);
        }
    }

    protected File getFile(GazelleTreeNodeImpl<XmlNodeData> node) throws IOException {
        int i = media.get(id(node));
        File f = downloads.get(i);
        GazelleTreeNodeImpl<XmlNodeData> mediaNode = getMediaNode(node);
        String b64 = mediaNode.getData().getText();
        String suffix = mediaNode.getData().getAttributes().get("mediaType").toString().replaceFirst(".*/([^/]+)$", ".$1");
        if (f==null) {
            f = new B64Exporter().toTempFile(b64, "fragment-" + downloads.size(), suffix);
            downloads.set(i,f);
        }
        return f;
    }
    protected GazelleTreeNodeImpl<XmlNodeData> getMediaNode(GazelleTreeNodeImpl<XmlNodeData> node) {
        return node;
    }

    public void downloadToFile(GazelleTreeNodeImpl<XmlNodeData> node) {
        try {
            nodeToFile(node,true);
        } catch (IOException e) {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR,"net.ihe.gazelle.document.cant-download-fragment",e);
        }
    }
    public void showFile(GazelleTreeNodeImpl<XmlNodeData> node) {
        try {
            nodeToFile(node,false);
        } catch (IOException e) {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR,"net.ihe.gazelle.document.cant-show-fragment",e);
        }
    }
    public String downloadFilename(GazelleTreeNodeImpl<XmlNodeData> node) {
        if (media.containsKey(id(node))) {
            return "Document-"+media.get(id(node))+"."+node.getData().getAttributes().get("mediaType").toString().replaceFirst(".*/([^/]+)$", "$1");
        } else {
            return "";
        }
    }

    protected XmlNodeData getMediaNodeData(GazelleTreeNodeImpl<XmlNodeData> node) {
        if (node==null) {
            return null;
        }
        GazelleTreeNodeImpl<XmlNodeData> mediaNode = getMediaNode(node);
        if (mediaNode==null) {
            return null;
        }
        return mediaNode.getData();
    }
    protected Boolean isB64NodeData(XmlNodeData data) {
        return data!=null && data.getAttributes()!=null
                && data.getAttributes().containsKey("representation")
                && "B64".equalsIgnoreCase(data.getAttributes().get("representation").toString());
    }

    private Boolean isDownloadableNode(GazelleTreeNodeImpl<XmlNodeData> node) {
        return isB64NodeData(getMediaNodeData(node));
    }

    private Boolean isValidableNode(GazelleTreeNodeImpl<XmlNodeData> node) {
        return isB64NodeData(getMediaNodeData(node));
    }

    private Boolean isValidatedNode(GazelleTreeNodeImpl<XmlNodeData> node) {
        return isB64NodeData(getMediaNodeData(node));
    }


    @Override
    public String renderNodeType(GazelleTreeNodeImpl<XmlNodeData> node) {
        if (isB64NodeData(getMediaNodeData(node))) {
            return "b64";
        }
        return "xml";
    }

    protected Long id(GazelleTreeNodeImpl<XmlNodeData> node) {
        HashCode hc = hashFunction.hashString(node.toString());
        return hc.asLong();
    }

}
