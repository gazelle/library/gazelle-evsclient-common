package net.ihe.gazelle.evsclient.interlay.gui.document;

import org.apache.commons.lang.StringEscapeUtils;

import javax.faces.model.SelectItem;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

public class DicomDocumentRenderer extends PlainDocumentRenderer {

    private DicomDumperSelector selector;

    public DicomDocumentRenderer() {
        super();
        this.selector = new DicomDumperSelector(Arrays.asList(new Pixelmed2txtDumper(), new Dcm2txtDumper(), new Dicom3tools2txtDumper()));
        this.options.hasRenderingModes = true;
        this.options.renderingMode = new Pixelmed2txtDumper().name();
        this.options.prettyView = true;
    }

    public List<SelectItem> getRenderingModes() {
        return selector.getDumpingModes();
    }

    @Override
    protected String prettify(HandledDocument doc) throws RenderingException {
        return selector.dump(this,doc);
    }

    @Override
    protected String highlight(String message, Charset encoding) throws RenderingException {
        return StringEscapeUtils.escapeHtml(message);
    }

    @Override
    protected String toString(HandledDocument doc) throws RenderingException {
        return ""; // cannot render binary dicom
    }

}
