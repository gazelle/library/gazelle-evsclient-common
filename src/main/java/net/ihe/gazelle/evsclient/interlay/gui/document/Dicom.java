package net.ihe.gazelle.evsclient.interlay.gui.document;

public abstract class Dicom  {

    public Dicom() {
    }

    /*
    private String executeDicomOperation(final DicomOperation selectedOperation, final String objectPath) {
        String resultValidation = null;
        if (selectedOperation != null) {
            final Pixelmed pixelmed = new Pixelmed();
            final Dicom4che dcm4che = new Dicom4che();
            Dicom3tools d3t = new Dicom3tools();
            DcmCheckValidator dcmcheck = new DcmCheckValidator();
            final DCCheck dccheck = new DCCheck();
            switch (selectedOperation) {
                case PIXELMED_VALIDATION:
                    try {
                        resultValidation = pixelmed.validateDicomSRDose(objectPath);
                        selectedOperation.setVersion(pixelmed.getVersion());
                    } catch (final Exception e) {
                        resultValidation =
                                "A problem occurred when trying to execute " + selectedOperation.getTool().getValue() + "::"
                                        + selectedOperation.getLabel() + '(' + pixelmed.getVersion() + "). " + e
                                        .getMessage();
                        DicomValidationManager.LOGGER.error(resultValidation);
                    }
                    break;

                case PIXELMED_DUMP:
                    try {
                        resultValidation = pixelmed.getDumpResult(objectPath);
                        selectedOperation.setVersion(pixelmed.getVersion());
                    } catch (final Exception e) {
                        resultValidation =
                                "A problem occurred when trying to execute " + selectedOperation.getTool().getValue() + "::"
                                        + selectedOperation.getLabel() + '(' + pixelmed.getVersion() + "). " + e
                                        .getMessage();
                        DicomValidationManager.LOGGER.error(resultValidation);
                    }
                    break;

                case PIXELMED_DUMP2XML:
                    try {
                        resultValidation = pixelmed.dicom2xml(objectPath);
                        selectedOperation.setVersion(pixelmed.getVersion());
                    } catch (final Exception e) {
                        resultValidation =
                                "A problem occured when trying to execute " + selectedOperation.getTool().getValue() + "::"
                                        + selectedOperation.getLabel() + '(' + pixelmed.getVersion() + "). " + e
                                        .getMessage();
                        DicomValidationManager.LOGGER.error(resultValidation);
                    }
                    break;

                case DCM4CHE_DUMP:
                    try {
                        resultValidation = dcm4che.dicom2txt(objectPath);
                        selectedOperation.setVersion(dcm4che.getVersion());
                    } catch (final IOException e) {
                        resultValidation =
                                "A problem occured when trying to execute " + selectedOperation.getTool().getValue() + "::"
                                        + selectedOperation.getLabel() + '(' + dcm4che.getVersion() + "). " + e
                                        .getMessage();
                        DicomValidationManager.LOGGER.error(resultValidation);
                    }
                    break;

                case DCM4CHE_DUMP2XML:
                    try {
                        resultValidation = dcm4che.dicom2xml(objectPath);
                        selectedOperation.setVersion(dcm4che.getVersion());
                    } catch (final TransformerConfigurationException|IOException e) {
                        resultValidation =
                                "A problem occured when trying to execute " + selectedOperation.getTool().getValue() + "::"
                                        + selectedOperation.getLabel() + '(' + dcm4che.getVersion() + "). " + e
                                        .getMessage();
                        DicomValidationManager.LOGGER.error(resultValidation);
                    }
                    break;

                case DICOM3TOOLS_DUMP:
                    try {
                        d3t = new Dicom3tools(this.selectedService.getWsdl());
                        resultValidation = d3t.dicomDump(objectPath);
                        selectedOperation.setVersion(d3t.getVersion());
                    } catch (final RuntimeException e) {
                        DicomValidationManager.LOGGER.error(e.getMessage());
                        resultValidation =
                                "A problem occured when trying to execute " + selectedOperation.getTool().getValue() + "::"
                                        + selectedOperation.getLabel() + '(' + d3t.getVersion() + "). " + e
                                        .getMessage();
                    }
                    break;

                case DICOM3TOOLS_VALIDATION:
                    try {
                        d3t = new Dicom3tools(this.selectedService.getWsdl());
                        resultValidation = d3t.validate(objectPath);
                        selectedOperation.setVersion(d3t.getVersion());
                    } catch (final RuntimeException e) {
                        DicomValidationManager.LOGGER.error(e.getMessage());
                        resultValidation =
                                "A problem occured when trying to execute " + selectedOperation.getTool().getValue() + "::"
                                        + selectedOperation.getLabel() + '(' + d3t.getVersion() + "). " + e.getMessage();
                    }
                    break;

                case DCMCHECK_VALIDATION:
                    try {
                        dcmcheck = new DcmCheckValidator(this.selectedService.getWsdl());
                        resultValidation = dcmcheck.validate(objectPath);
                    } catch (final RuntimeException e) {
                        DicomValidationManager.LOGGER.error(e.getMessage());
                        resultValidation =
                                "A problem occured when trying to execute " + selectedOperation.getTool().getValue() + "::"
                                        + selectedOperation.getLabel() + '(' + dcmcheck.getVersion() + "). " + e
                                        .getMessage();
                    }
                    break;
                case DCCHECK_VALIDATION:
                    try {
                        final String inValidatorEndpoint = this.selectedService.getWsdl();
                        final String xslLocation = this.selectedService.getXslLocation();
                        resultValidation = dccheck.validate(inValidatorEndpoint, xslLocation, objectPath);
                    } catch (final RuntimeException e) {
                        DicomValidationManager.LOGGER.error(e.getMessage());
                        resultValidation =
                                "A problem occure when trying to execute " + selectedOperation.getTool().getValue() + "::"
                                        + selectedOperation.getLabel() + ". Error: " + e;
                    }
                    break;
                case DVTK_VALIDATION:
                    try {
                        final String inValidatorEndpoint = this.selectedService.getWsdl();
                        final String xslLocation = this.selectedService.getXslLocation();
                        final DVTK dvtk = new DVTK();
                        resultValidation = dvtk.validate(inValidatorEndpoint, xslLocation, objectPath);
                    } catch (final RuntimeException e) {
                        DicomValidationManager.LOGGER.error(e.getMessage());
                        resultValidation =
                                "A problem occured when trying to execute " + selectedOperation.getTool().getValue() + "::"
                                        + selectedOperation.getLabel() + ". Error: " + e;
                    }
                    break;
                default:
                    break;
            }
        }
        if (resultValidation != null) {
            resultValidation = resultValidation.replace("\0", "");
        }
        return resultValidation;
    }
     */
}
