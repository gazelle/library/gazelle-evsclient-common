package net.ihe.gazelle.evsclient.interlay.gui.document;

import gnu.trove.map.hash.THashMap;
import org.apache.commons.lang.exception.ExceptionUtils;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class DicomDumperSelector {

    private Map<String, DicomDumper> dumpers;
    private List<SelectItem> items;

    public DicomDumperSelector(List<DicomDumper> dumpers) {
        super();
        this.dumpers = new THashMap<>();
        this.items = new ArrayList<>();
        for (DicomDumper dumper : dumpers) {
            this.dumpers.put(dumper.name(), dumper);
            SelectItem selectItem = new SelectItem();
            selectItem.setValue(dumper.name());
            selectItem.setLabel(dumper.name());
            if (!dumper.isAvailable()) {
                selectItem.setDisabled(true);
            }
            items.add(selectItem);
        }
    }

    public List<SelectItem> getDumpingModes() {
        return items;
    }

    public SelectItem getDumpingMode(String name) {
        for (SelectItem item : items) {
            if (item.getLabel().equals(name)) {
                return item;
            }
        }
        return null;
    }

    public DicomDumper get(String name) {
        return dumpers.get(name);
    }


    public String dump(AbstractRenderer renderer, HandledDocument doc) throws Renderer.RenderingException {
        DicomDumper dumper = this.get(renderer.options.renderingMode);
        try {
            return dumper.dump(renderer.toByteArray(doc));
        } catch (Renderer.RenderingException e) {
            /*
            // try with next dumper if any
            int i = getDumpingModes().indexOf(this.getDumpingMode(renderer.options.renderingMode));
            if (i>=0 && i<this.getDumpingModes().size()-1) {
                renderer.options.renderingMode = this.getDumpingModes().get(i+1).getLabel();
                return dump(renderer,doc);
            } else {
                throw e;
            }
            */
            return ExceptionUtils.getStackTrace(e);
        }
    }
}
