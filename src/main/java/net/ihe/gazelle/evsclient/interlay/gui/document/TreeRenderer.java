package net.ihe.gazelle.evsclient.interlay.gui.document;

import org.richfaces.model.TreeNode;

public interface TreeRenderer<T extends TreeNode,O extends TreeRenderer.Options<?>> extends Renderer<T,O> {
    interface Options<A extends TreeRenderer.Options.Actions> extends Renderer.Options {
        interface Actions {

        }
        Boolean getExpandsAll();
        Boolean hasExpandAllMode();
        Boolean hasFilterMode();
        A getActions();
    }

    void setNodeFilter(String filter);
    String getNodeFilter();
    Boolean isRenderedNode(T node);
    String renderNode(T node);
    String renderNodeType(T node);
}
