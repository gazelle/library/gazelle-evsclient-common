package net.ihe.gazelle.evsclient.interlay.dao;

public class FileReadException  extends RuntimeException {
   private static final long serialVersionUID = 1297682890709921708L;

   public FileReadException(String s, Throwable throwable) {
      super(s, throwable);
   }
}
