package net.ihe.gazelle.evsclient.interlay.dao;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.NotLoadedException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingDao;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotFoundException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotSavedException;
import net.ihe.gazelle.evsclient.domain.processing.AbstractProcessing;
import net.ihe.gazelle.evsclient.domain.processing.EntryPoint;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.processing.UnexpectedProcessingException;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.util.Date;
import java.util.List;


public abstract class AbstractProcessingDaoImpl<T extends AbstractProcessing> implements ProcessingDao<T> {

   protected ApplicationPreferenceManager applicationPreferenceManager;
   protected EntityManagerFactory entityManagerFactory;
   protected HandledObjectFileDaoImpl handledObjectDao;

   public AbstractProcessingDaoImpl(EntityManagerFactory entityManagerFactory,
                                    ApplicationPreferenceManager applicationPreferenceManager) {
      this.applicationPreferenceManager = applicationPreferenceManager;
      this.entityManagerFactory = entityManagerFactory;
      this.handledObjectDao = new HandledObjectFileDaoImpl(
            applicationPreferenceManager); // not injected because of package visibility
   }

   public abstract Repository getRepositoryType(T processedObject);

   @Override
   public FilterDataModel<T> getFilteredDataModel(Filter<T> filter) {
      return new FilterDataModel<T>(filter) {
         private static final long serialVersionUID = -8349383907357456509L;
         @Override
         protected Object getId(T processing) {
            return processing.getId();
         }
      };
   }

   @Override
   public T merge(T processing) {
      EntityManager entityManager = getEntityManager();
      processing = entityManager.merge(processing);
      entityManager.flush();
      return processing;
   }

   @Override
   public T create(T processing)
         throws UnexpectedProcessingException, ProcessingNotSavedException, NotLoadedException {

      EntityManager entityManager = getEntityManager();
      entityManager.persist(processing);

      handledObjectDao.saveObjectsInFiles(processing, getRepositoryType(processing));
      processing = entityManager.merge(processing);

      //TODO : catch exception at flush, delete file and throw exception
      entityManager.flush();
      return processing;
   }

   @Override
   public T getObjectById(final Integer id, final String privacyKey, boolean addSecurityRestriction,
                          GazelleIdentity identity) {
      final Criteria criteria = getCriteriaForObject(getEntityManager());
      addPrivacyOrSecurityRestriction(privacyKey, addSecurityRestriction, identity, criteria);
      return getUniqueObjectBy("id", id, criteria);
   }

   @Override
   public T getObjectById(final Integer id) {
      final Criteria criteria = getCriteriaForObject(getEntityManager());
      return getUniqueObjectBy("id", id, criteria);
   }

   @Override
   public T getObjectByOid(final String oid, final String privacyKey, boolean addSecurityRestriction,
                           GazelleIdentity identity) {
      final Criteria criteria = getCriteriaForObject(getEntityManager());
      addPrivacyOrSecurityRestriction(privacyKey, addSecurityRestriction, identity, criteria);
      return getUniqueObjectBy("oid", oid, criteria);

   }

   @Override
   public T getObjectByOid(final String oid) {
      final Criteria criteria = getCriteriaForObject(getEntityManager());
      return getUniqueObjectBy("oid", oid, criteria);
   }

   @Override
   public T getObjectByToolObjectIdAndToolOid(String toolObjectId, String toolOid) {
      final Criteria c = getCriteriaForObject(getEntityManager());

      c.createAlias("caller", "callerAlias")
            .add(Restrictions.eqOrIsNull("callerAlias.toolObjectId", toolObjectId))
            .add(Restrictions.eqOrIsNull("callerAlias.toolOid", toolOid))
            .add(Restrictions.isNotNull("date"))
            .addOrder(Order.desc("date"));

      return getFirstObject(c);
   }

   @Override
   public int countByEntryPoint(EntryPoint entryPoint) {
      Class<T> clazz = (Class<T>) ((ParameterizedType) getClass()
            .getGenericSuperclass()).getActualTypeArguments()[0];
      EntityManager entityManager = getEntityManager();

      final HQLQueryBuilder<T> queryBuilder = new HQLQueryBuilder<>(entityManager, clazz);
      queryBuilder.addRestriction(HQLRestrictions.eq("caller.entryPoint", entryPoint));

      return queryBuilder.getCount();
   }

   @Override
   public int countAll() {
      Class<T> clazz = (Class<T>) ((ParameterizedType) getClass()
            .getGenericSuperclass()).getActualTypeArguments()[0];
      EntityManager entityManager = getEntityManager();

      final HQLQueryBuilder<T> queryBuilder = new HQLQueryBuilder<>(entityManager, clazz);

      return queryBuilder.getCount();
   }

   @Override
   public List<T> findALlByDateBetween(Date startDate, Date endDate) {
      Class<T> clazz = (Class<T>) ((ParameterizedType) getClass()
            .getGenericSuperclass()).getActualTypeArguments()[0];
      EntityManager entityManager = getEntityManager();

      final HQLQueryBuilder<T> queryBuilder = new HQLQueryBuilder<>(entityManager, clazz);
      queryBuilder.addRestriction(HQLRestrictions.ge("date", startDate));
      queryBuilder.addRestriction(HQLRestrictions.le("date", endDate));
      queryBuilder.addOrder("date", false);
      return queryBuilder.getList();
   }

   private T getUniqueObjectBy(final String propertyName, final Object propertyValue, Criteria criteria) {
      if (propertyValue != null) {
         criteria.add(Restrictions.eqOrIsNull(propertyName, propertyValue));
         return getFirstObject(criteria);
      } else {
         return null;
      }
   }

   protected T getFirstObject(Criteria criteria) {
      final List<T> objects = criteria.list();
      return !objects.isEmpty() ? objects.get(0) : null;
   }

   protected Criteria getCriteriaForObject(EntityManager entityManager) {
      Class<T> clazz = (Class<T>) ((ParameterizedType) getClass()
            .getGenericSuperclass()).getActualTypeArguments()[0];

      final Session s = (Session) entityManager.getDelegate();
      return s.createCriteria(clazz);
   }

   protected void addPrivacyOrSecurityRestriction(String privacyKey, boolean addSecurityRestriction, GazelleIdentity identity,
                                                Criteria criteria) {
      if (addSecurityRestriction) {
         addSecurityRestriction(criteria, identity);
      } else {
         criteria.createAlias("sharing", "shrg")
               .add(Restrictions.eqOrIsNull("shrg.privacyKey", privacyKey));
      }
   }

   private void addSecurityRestriction(final Criteria criteria, GazelleIdentity identity) {
      // show all messages if monitor else show only public results and those performed by the logged in user
      boolean isMonitorOrAdmin = identity.hasRole("monitor_role") || identity.hasRole("admin_role");
      if (!isMonitorOrAdmin) {

         criteria.createAlias("sharing", "shrg")
               .createAlias("owner", "ownr");

         if (!identity.isLoggedIn()) {
            criteria.add(Restrictions.eqOrIsNull("shrg.isPrivate", Boolean.FALSE));
         } else if (applicationPreferenceManager.getBooleanValue("show_logs_from_colleagues")
               .booleanValue()) {
            criteria.add(Restrictions.or(
                  Restrictions.eqOrIsNull("shrg.isPrivate", Boolean.FALSE),
                  Restrictions.eqOrIsNull("ownr.username", identity.getUsername()),
                  Restrictions.eqOrIsNull("ownr.organization", identity.getOrganisationKeyword())
            ));
         } else {
            criteria.add(Restrictions.or(
                  Restrictions.eqOrIsNull("shrg.isPrivate", Boolean.FALSE),
                  Restrictions.eqOrIsNull("ownr.username", identity.getUsername())
            ));
         }
      }
   }

   protected abstract void deleteSpecificItems(T processing)
         throws ProcessingNotFoundException, UnexpectedProcessingException;

   @Override
   public void delete(String oid) throws ProcessingNotFoundException, UnexpectedProcessingException {
      try {
         T processing = getObjectByOid(oid);

         for (HandledObject object : processing.getObjects()) {
            handledObjectDao.deleteContentFile((HandledObjectFile) object);
         }
         deleteSpecificItems(processing);
         EntityManager entityManager = getEntityManager();

         T objectToDelete = entityManager.find((Class<T>) ((ParameterizedType) getClass()
               .getGenericSuperclass()).getActualTypeArguments()[0], processing.getId());
         entityManager.remove(objectToDelete);
      } catch (IOException e) {
         throw new UnexpectedProcessingException(e);
      }


   }

   protected EntityManager getEntityManager() {
      return entityManagerFactory.createEntityManager();
   }

}
