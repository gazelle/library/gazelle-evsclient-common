package net.ihe.gazelle.evsclient.interlay.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileUtil {

    private FileUtil() {}

    public static void createFileAndParentDirectories(byte[] fileContent, String filePath) throws IOException {
        final File fileFile = new File(filePath);
        createParentDirectories(fileFile);
        try (FileOutputStream fos = new FileOutputStream(fileFile)) {
            fos.write(fileContent);
        }
    }

    public static void createParentDirectories(File childFile) throws IOException {
        if (childFile != null) {
            final File parentFile = childFile.getParentFile();
            if (!parentFile.exists() && !parentFile.mkdirs()) {
                throw new IOException("Unable to create Directory : " + parentFile.getPath());
            }
        }
    }
}
