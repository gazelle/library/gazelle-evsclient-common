package net.ihe.gazelle.evsclient.interlay.gui.document;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.evsclient.interlay.gui.I18n;
import org.apache.commons.lang.StringEscapeUtils;

import java.nio.charset.Charset;
import java.util.Arrays;

public class BinDocumentRenderer extends AbstractDocumentRenderer<AbstractDocumentRenderer.Options> {
    private static final String[] hexSymbols = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };

    public static final int BITS_PER_HEX_DIGIT = 4;

    public static String toHexFromByte(final byte b)
    {
        byte leftSymbol = (byte)((b >>> BITS_PER_HEX_DIGIT) & 0x0f);
        byte rightSymbol = (byte)(b & 0x0f);

        return (hexSymbols[leftSymbol] + hexSymbols[rightSymbol]);
    }

    public String toHexFromBytes(final byte[] bytes)
    {
        if(bytes == null || bytes.length == 0)
        {
            return ("");
        }

        // there are 2 hex digits per byte
        StringBuilder hexBuffer = new StringBuilder();
        StringBuilder hex = new StringBuilder();
        StringBuilder ascii = new StringBuilder();

        // for each byte, convert it to hex and append it to the buffer
        for(int i = 0; i < bytes.length; i++)
        {
            if (i>0&&i%options.maxColumns ==0) {
                hexBuffer
                        .append(hex)
                        .append("\t")
                        .append(ascii)
                        .append("\n");
                hex = new StringBuilder();
                ascii = new StringBuilder();
            }
            hex.append(toHexFromByte(bytes[i])).append(" ");
            ascii.append(bytes[i]>31&&bytes[i]<127?(char)bytes[i]:"\u2022");
        }
        hexBuffer.append(hex);
        if ((options.maxColumns -ascii.length())>0) {
            char[] chars = new char[(options.maxColumns -ascii.length()) * 3];
            Arrays.fill(chars, ' ');
            hexBuffer.append(new String(chars));
        }
        hexBuffer.append("\t").append(ascii);
        return (hexBuffer.toString());
    }


    public BinDocumentRenderer() {
        super(new Options(true),AbstractDocumentRenderer.Options.class);
        this.options.hasPrettyViewMode=false;
        this.options.prettyView=true;
        this.options.hasMaxColumnsMode=true;
        try {
            options.maxColumns = ApplicationPreferenceManagerImpl.instance().getIntegerValue("document_bin_view_mode_columns");
        } catch (Exception e) {
            options.maxColumns = 80;
        }
    }

    @Override
    public String render(HandledDocument doc) {
        if (options.prettyView) {
            return super.render(doc);
        } else {
            return I18n.get("net.ihe.gazelle.document.cant-show-binary-content");
        }
    }

    @Override
    protected String prettify(HandledDocument doc) throws RenderingException {
        return toHexFromBytes(doc.getContent());
    }

    @Override
    protected String highlight(String message, Charset encoding) throws RenderingException {
        StringBuilder sb = new StringBuilder("<code>");
        sb.append(StringEscapeUtils.escapeHtml(message)
                .replace("\n","</code>\n<code>")
                .replace("\t","&nbsp;&nbsp;")
                .replace(" ","&#8239;")
        );
        sb.append("</code>");
        return sb.toString();
    }

}
