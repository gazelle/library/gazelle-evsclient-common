package net.ihe.gazelle.evsclient.interlay.gui.document;

import net.ihe.gazelle.evsclient.interlay.gui.I18n;
import org.apache.commons.lang.StringUtils;

import java.net.URI;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Iterator;

public class UrlDocumentRenderer extends AbstractDocumentRenderer<UrlDocumentRenderer.Options> {

    public static class Options extends AbstractDocumentRenderer.Options {
        protected Boolean decode;

        public Options(boolean initDefaultValues) {
            super(initDefaultValues);
            if (initDefaultValues) {
                this.decode = true;
            }
        }
        public Options() {
            this(false);
        }

        public Boolean getDecode() {
            return decode;
        }

        public void setDecode(Boolean decode) {
            this.decode = decode;
        }

        @Override
        protected <X> X merge(X options) {
            UrlDocumentRenderer.Options opt = super.merge((UrlDocumentRenderer.Options) options);
            opt.decode = opt.decode==null?this.decode:opt.decode;
            return (X)opt;
        }
    }


    public UrlDocumentRenderer() {
        super(new UrlDocumentRenderer.Options(true), UrlDocumentRenderer.Options.class);
        this.options.hasViewLineNumberMode = false;
        this.options.viewLineNumber = false;
    }
    @Override
    protected String prettify(HandledDocument doc) throws RenderingException {
        return toString(doc);
    }

    @Override
    protected String highlight(String message, Charset encoding) throws RenderingException {
        try {
            URI uri = new URI(message);
            StringBuilder sb = new StringBuilder();
            highlightComponent(sb, "scheme", uri.getScheme(),"://");
            if (StringUtils.isNotEmpty(uri.getAuthority())) {
                if (StringUtils.isNotEmpty(uri.getRawUserInfo())) {
                    highlightComponent(sb, "userinfo", uri.getUserInfo(),"@");
                }
                boolean hasPort = uri.getRawAuthority().matches(".*?\\:[0-9]+$");
                if (StringUtils.isNotEmpty(uri.getHost())) {
                    highlightComponent(sb, "host", uri.getHost(),hasPort?":":"/");
                }
                if (hasPort) {
                    highlightComponent(sb, "port", Integer.toString(uri.getPort()),"/");
                }
            }
            boolean hasQueryPart = StringUtils.isNotEmpty(uri.getRawQuery());
            boolean hasFragmentPart = StringUtils.isNotEmpty(uri.getFragment());
            highlightComponent(sb, "path", uri.getPath().replaceFirst("^/",""),hasQueryPart?"?":(hasFragmentPart?"#":""));
            if (hasQueryPart) {
                Iterator<String> queryParts = Arrays.asList(uri.getRawQuery().split("[&;]")).iterator();
                while (queryParts.hasNext()) {
                    String[] parameter = queryParts.next().split("=");
                    highlightComponent(sb, "query-parameter-name", parameter[0],"=");
                    highlightComponent(sb, "query-parameter-value", parameter[1],queryParts.hasNext()?"&":(hasFragmentPart?"#":""));
                }
            }
            if (hasFragmentPart) {
                highlightComponent(sb, "fragment", uri.getRawFragment(),"");
            }
            return sb.toString();
        } catch (Exception e) {
            throw new RenderingException(e);
        }
    }

    private void highlightComponent(StringBuilder sb, String token, String value, String separator) {
        sb.append("<span class=\"url-").append(token).append("\">").append(options.decode?URLDecoder.decode(value):value).append("</span>");
        if (StringUtils.isNotEmpty(separator)) {
          sb.append("<span class=\"url-separator\">").append(separator).append("</span>");
        }
    }

}
