package net.ihe.gazelle.evsclient.interlay.gui.document;

import com.jayway.jsonpath.PathNotFoundException;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.evsclient.interlay.gui.I18n;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.jfree.util.Log;
import org.richfaces.model.TreeNode;

import java.util.Map;

public abstract class AbstractTreeRenderer<T extends TreeNode, O extends AbstractTreeRenderer.Options> extends AbstractRenderer<T,O> implements TreeRenderer<T,O> {

    public static class Options extends AbstractRenderer.Options implements TreeRenderer.Options<Options.Actions> {
        public static class Actions implements TreeRenderer.Options.Actions {
            protected Boolean download;
            protected Boolean show;
            protected Boolean validation;
            protected Boolean validate;

            public Actions(boolean initDefaultValues) {
                super();
                if (initDefaultValues) {
                    download = true;
                    show = true;
                    validation = true;
                    validate = true;
                }
            }
            public Actions() {
                this(false);
            }


            public Boolean getDownload() {
                return download;
            }

            public Boolean getShow() {
                return show;
            }

            public Boolean getValidation() {
                return validation;
            }

            public Boolean getValidate() {
                return validate;
            }
        }

        protected Boolean expandsAll;
        protected Boolean hasExpandAllMode;
        protected Boolean hasFilterMode;

        protected Actions actions;

        public Options(boolean initDefaultValues) {
            super(initDefaultValues);
            if (initDefaultValues) {
                expandsAll = true;
                hasExpandAllMode = true;
                hasFilterMode = true;
            }
            actions = new Actions(initDefaultValues);
        }
        public Options() {
            this(false);
        }

        @Override
        public Boolean getExpandsAll() {
            return expandsAll;
        }

        public void setExpandsAll(Boolean expandsAll) {
            this.expandsAll = expandsAll;
        }

        @Override
        public Boolean hasExpandAllMode() {
            return hasExpandAllMode;
        }

        @Override
        public Boolean hasFilterMode() {
            return hasFilterMode;
        }

        public Actions getActions() {
            return actions;
        }

        @Override
        protected <X> X merge(X options) {
            AbstractTreeRenderer.Options opt = super.merge((AbstractTreeRenderer.Options) options);
            opt.expandsAll = opt.expandsAll==null?this.expandsAll:opt.expandsAll;
            opt.hasExpandAllMode = opt.hasExpandAllMode==null?this.hasExpandAllMode:opt.hasExpandAllMode;
            opt.hasFilterMode = opt.hasFilterMode==null?this.hasFilterMode:opt.hasFilterMode;
            opt.actions.download = opt.actions.download==null?this.actions.download:opt.actions.download;
            opt.actions.show = opt.actions.show==null?this.actions.show:opt.actions.show;
            opt.actions.validation = opt.actions.validation==null?this.actions.validation:opt.actions.validation;
            opt.actions.validate = opt.actions.validate==null?this.actions.validate:opt.actions.validate;
            return (X)opt;
        }
    }

    protected T tree;
    protected String nodeFilter;

    protected AbstractTreeRenderer(O options,Class<O> optionsClass) {
        super(options,optionsClass);
        try {
            Map<String, Object> values = ApplicationPreferenceManagerImpl.instance().getValues();
            if (values.containsKey("document_expands_all_nodes")) {
                this.options.expandsAll = ApplicationPreferenceManagerImpl.instance().getBooleanValue("document_expands_all_nodes");
            } else {
                this.options.expandsAll = true;
            }
            if (values.containsKey("has_filter_mode")) {
                this.options.hasFilterMode = ApplicationPreferenceManagerImpl.instance().getBooleanValue("has_filter_mode");
            } else {
                this.options.hasFilterMode = true;
            }
        } catch (Exception e) {
            this.options.hasFilterMode = true;
        }
    }

    public T render(HandledDocument doc) {
        if (doc == null) {
            return emptyRendering();
        }
        try {
            return toTreeNode(doc);
        } catch (Exception e) {
            String warningMessage = I18n.get("net.ihe.gazelle.document.unable-to-render", doc.getSyntax(), e.getMessage());
            Log.warn(warningMessage, e);
            String warningCaption = I18n.get("net.ihe.gazelle.evs.cannotRenderTreeView");
            this.warningCaption += "</br></br><span class=\"rendering-error\"> "+ warningCaption + "</span>";
            return null;
        }
    }

    @Override
    public void setNodeFilter(String filter) {
        if (StringUtils.isNotEmpty(filter)) {
            try {
                this.nodeFilter = filter;
            } catch (Exception e) {
                this.nodeFilter = null;
            }
        } else {
            this.nodeFilter = null;
        }
        tree=null;
    }

    @Override
    public String getNodeFilter() {
        if (nodeFilter==null) {
            return "";
        }
        return nodeFilter;
    }

    @Override
    public Boolean isRenderedNode(T node) {
        return true;
    }

    public abstract Boolean isActionAppropriate(String action, T node);

    protected abstract T toTreeNode(HandledDocument doc) throws RenderingException;

    public String getCode() {
        return this.getClass().getSimpleName();
    }

    @Override
    public Boolean isTree() {
        return true;
    }

    public String getFilterLabel(){
        return I18n.get("net.ihe.gazelle.document.xpath-expression");
    }

}
