package net.ihe.gazelle.evsclient.interlay.gui.processing;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.CallerMetadataFactory;
import net.ihe.gazelle.evsclient.application.interfaces.ForbiddenAccessException;
import net.ihe.gazelle.evsclient.application.interfaces.GuiPermanentLink;
import net.ihe.gazelle.evsclient.application.interfaces.NotFoundException;
import net.ihe.gazelle.evsclient.application.interfaces.ProcessingManager;
import net.ihe.gazelle.evsclient.application.interfaces.UnauthorizedException;
import net.ihe.gazelle.evsclient.domain.processing.*;
import net.ihe.gazelle.evsclient.interlay.gui.QueryParam;
import net.ihe.gazelle.evsclient.interlay.gui.document.HandledDocument;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.Component;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public abstract class AbstractProcessingBeanGui<T extends AbstractProcessing, M extends ProcessingManager<T>>
        extends FileProcessingBeanGui
        implements GuiPermanentLink<T>, UserAttributeCommon {

    protected M processingManager;

    protected T selectedObject;

    protected final CallerMetadataFactory callerMetadataFactory;

    protected GazelleIdentity identity;

    protected ApplicationPreferenceManager applicationPreferenceManager;

    protected GuiPermanentLink<T> permanentLinkGui;

    protected String externalId;

    protected String proxyType;

    protected String toolOid;

    protected String callerPrivacyKey;

    private final UserService userService = (UserService) Component.getInstance("gumUserService");

    protected AbstractProcessingBeanGui(Class<?> cls, M processingManager, CallerMetadataFactory callerMetadataFactory, GazelleIdentity identity, ApplicationPreferenceManager applicationPreferenceManager, GuiPermanentLink<T> permanentLinkGui) {
        super(cls);
        this.processingManager = processingManager;
        this.callerMetadataFactory = callerMetadataFactory;
        this.identity = identity;
        this.applicationPreferenceManager = applicationPreferenceManager;
        this.permanentLinkGui = permanentLinkGui;

        final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        this.toolOid = urlParams.get(QueryParam.TOOL_OID);
        this.externalId = urlParams.get(QueryParam.EXTERNAL_ID);
        this.proxyType = urlParams.get(QueryParam.PROXY_TYPE);
        this.callerPrivacyKey = urlParams.get(QueryParam.CALLER_PRIVACY_KEY);
    }

    public T loadProcessingObjectByOid(final String objectOid, final String privacyKey)
          throws UnauthorizedException, NotFoundException, ForbiddenAccessException {
        return processingManager.getObjectByOID(objectOid, privacyKey, identity);
    }

    public void initFromUrl() throws UnauthorizedException, NotFoundException, ForbiddenAccessException {
        final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams != null && !urlParams.isEmpty()) {
            if(urlParams.containsKey(QueryParam.PROCESSING_ID)) {
                setSelectedObject(processingManager.getObjectByID(Integer.valueOf(urlParams.get(QueryParam.PROCESSING_ID)),
                                                                 urlParams.get(QueryParam.PRIVACY_KEY),
                        identity));
            } else if(urlParams.containsKey(QueryParam.PROCESSING_OID)) {
                setSelectedObject(loadProcessingObjectByOid(urlParams.get(QueryParam.PROCESSING_OID),
                                                           urlParams.get(QueryParam.PRIVACY_KEY)));
            }
        }
    }
    public void setSelectedObject(T so) {
        this.selectedObject = so;
        if (this.selectedObject == null) {
            this.setDocument(null);
        } else {
            HandledObject ho = this.selectedObject.getObject();
            if (ho != null && ho.getContent() != null) {
                this.setDocument(new HandledDocument(ho.getContent()));
            } else {
                this.setDocument(null);
            }
        }
    }

    public EVSCallerMetadata getEvsCallerMetadata() {
        return callerMetadataFactory.getCallerMetadata(toolOid,
                                                       externalId,
                                                       proxyType,
                                                       EntryPoint.GUI);
    }

    public OwnerMetadata getOwnerMetadata(){
        if(identity.isLoggedIn()){
            return new OwnerMetadata(getUserIpAddress(), identity.getUsername(), identity.getOrganisationKeyword());
        }
        return new OwnerMetadata(getUserIpAddress(), null, null);
    }

    public static String getUserIpAddress() {
        return ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest())
                .getRemoteAddr();
    }

    public T getSelectedObject() {
        return this.selectedObject;
    }

    public void makeResultPublic() {
        processingManager.makeResultPublic(selectedObject);
    }

    public void makeResultPrivate() {
        processingManager.makeResultPrivate(selectedObject);
    }

    @Override
    public String getPermanentLink(T selectedObject) {
        return permanentLinkGui.getPermanentLink(selectedObject);
    }

    @Override
    public String getUserName(String userID) {
        return userService.getUserDisplayNameWithoutException(userID);
    }
}
