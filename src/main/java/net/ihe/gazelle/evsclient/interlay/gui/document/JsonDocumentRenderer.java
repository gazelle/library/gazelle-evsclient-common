package net.ihe.gazelle.evsclient.interlay.gui.document;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uwyn.jhighlight.renderer.JavaScriptXhtmlRenderer;

import java.io.IOException;
import java.nio.charset.Charset;

public class JsonDocumentRenderer extends AbstractDocumentRenderer {
    public JsonDocumentRenderer() {
        super(new AbstractDocumentRenderer.Options(true),AbstractDocumentRenderer.Options.class);
    }

    @Override
    protected String prettify(HandledDocument doc) throws RenderingException {
        ObjectMapper om = new ObjectMapper();
        try {
            return om.writerWithDefaultPrettyPrinter()
                    .writeValueAsString(
                        om.readTree(
                                doc.getContent()));
        } catch (IOException e) {
            throw new RenderingException(e);
        }
    }

    @Override
    protected String highlight(String message, Charset encoding) throws RenderingException {
        try {
            return new JavaScriptXhtmlRenderer().highlight("json", message, encoding.name(), true);
        } catch (IOException e) {
            throw new RenderingException(e);
        }
    }
}
