package net.ihe.gazelle.evsclient.interlay.gui.document;

import org.dcm4che2.tool.dcm2txt.Dcm2Txt;

import javax.xml.transform.TransformerConfigurationException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

public class Dcm2txtDumper extends DicomDumper {
    @Override
    protected String dump(String dicomFilePath) throws TransformerConfigurationException, IOException {
        PrintStream ops = System.out;
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        try {
            Dcm2Txt dcm2txt = new Dcm2Txt();
            PrintStream nps = new PrintStream(outStream, true, StandardCharsets.UTF_8.name());
            System.setOut(nps);
            dcm2txt.dump(new File(dicomFilePath));
            return outStream.toString(StandardCharsets.UTF_8.name());
        } finally {
            System.setOut(ops);
            outStream.close();
        }
    }

    @Override
    protected String name() {
        return "Dicom4che";
    }
}
