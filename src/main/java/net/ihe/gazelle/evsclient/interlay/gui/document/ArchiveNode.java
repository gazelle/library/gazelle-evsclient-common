package net.ihe.gazelle.evsclient.interlay.gui.document;

public interface ArchiveNode {
    String name();
}
