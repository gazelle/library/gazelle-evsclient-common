package net.ihe.gazelle.evsclient.interlay.gui.document;

import org.apache.commons.lang.StringEscapeUtils;

import java.nio.charset.Charset;

public class ZipDocumentRenderer extends BinDocumentRenderer {

    public ZipDocumentRenderer() {
        super();
        this.options.hasViewLineNumberMode = false;
        this.options.hasPrettyViewMode = false;
        this.options.viewLineNumber = false;
        this.options.hasMaxColumnsMode = false;
    }

    @Override
    protected String prettify(HandledDocument doc) throws RenderingException {
        return new ZipLister(doc.getContent()).list();
    }

    @Override
    protected String highlight(String message, Charset encoding) throws RenderingException {
        String tmp = StringEscapeUtils.escapeHtml(message);
        while (tmp.contains("\t")) {
            tmp = tmp.replaceFirst("\\t", "&nbsp;</td><td style=\"text-align: right;\">&nbsp;")
                    .replaceFirst("\\t", "&nbsp;</td><td>&nbsp;&nbsp;");
        }
        return tmp;
    }
}
