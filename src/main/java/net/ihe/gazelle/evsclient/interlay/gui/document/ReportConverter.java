/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.interlay.gui.document;

import net.ihe.gazelle.common.report.ReportExporterManager;
import net.ihe.gazelle.evsclient.interlay.factory.DocumentBuilderFactoryException;
import net.ihe.gazelle.evsclient.interlay.factory.XmlDocumentBuilderFactory;
import net.ihe.gazelle.preferences.PreferenceService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.input.BOMInputStream;
import org.dom4j.Document;
import org.dom4j.Node;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ReportConverter {

    public static final String DESCRIPTION = "Description";
    public static final String ERROR = "Error";
    public static final String RESULT_XML = "ResultXML";
    public static final String LOCATION = "Location";
    public static final String WARNING = "Warning";
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportConverter.class);
    protected static final Charset ENCODING = StandardCharsets.UTF_8;
    private static final String ERROR_PERFORMING_XSL_TRANSFORMATION = "Error performing XSL transformation of XML file : ";
    public static final String REQUESTING_XSL_TRANSFORMATION_OF_IN_FILE_PATH_BEING_NULL_OR_EMPTY = " Requesting XSL transformation of inFilePath being null or empty";

    /**
     * Takes a file and copies its content into a string which is returned
     *
     * @param inFile: File object to be read and returned as a string
     * @return a string with the content of the file. Null is something wrong happends
     * @deprecated This method copy the file line per line and add a line feed at the end of the file.
     */
    @Deprecated
    public static String getFileContentToString(final File inFile) {
        if (inFile == null) {
            return null;
        }
        return getFileContentToString(Paths.get(inFile.toURI()));
    }


    /**
     * Creates a file from its path and calls the method which returns the string
     *
     * @param filePath: path of the file to read and return the content into a string
     * @return string with the content of the file. Null if something wrong happends
     */
    public static String getFileContentToString(final String filePath) {
        if (filePath == null || filePath.isEmpty()) {
            return null;
        }
        return getFileContentToString(Paths.get(filePath));
    }

    public static String getFileContentToString(final Path filePath) {
        if (filePath == null) {
            return null;
        }
        try {
            // skip Byte order mark
            return new ContentConverter().toString(
                            Files.readAllBytes(filePath));
        } catch (IOException e) {
            ReportConverter.LOGGER.error(e.getMessage());
            return null;
        }

    }



    /**
     * Takes two strings and the corresponding patterns, concats them and returns the corresponding java.util.Date
     *
     * @param inStringDay  a string representing the date
     * @param inDayFormat  the pattern used to write the inStringDay
     * @param inStringTime a string representing the time
     * @param inTimeFormat the pattern used to write the inStringTime
     */
    public static Date convertStringToDate(final String inStringDay, final String inDayFormat, final String inStringTime,
                                           final String inTimeFormat, final Locale inLocal) {
        final String stringDate = inStringDay.concat("/").concat(inStringTime);
        final String dateFormat = inDayFormat.concat("/").concat(inTimeFormat);
        final DateFormat df = new SimpleDateFormat(dateFormat, inLocal);
        try {
            return df.parse(stringDate);
        } catch (final ParseException e) {
            ReportConverter.LOGGER.error("given string ({}) does not match the pattern: {}", stringDate, dateFormat);
            return null;
        } catch (final IllegalArgumentException e) {
            ReportConverter.LOGGER.error(e.getMessage());
            return null;
        }
    }

    public static String convertDateToString(final Date validationDate) {
        if (validationDate == null) {
            return null;
        }
        final SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy, MM dd - hh:mm (aa)", Locale.getDefault());
        return sdfDate.format(validationDate);
    }


    private static Transformer prepareTransformation(final String inXslPath) throws TransformerConfigurationException {

        Transformer transformer = TransformerCache.instance().getTransformerFromCache(inXslPath);
        if (transformer == null) {
            return null;
        }

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, StandardCharsets.UTF_8.name());
        return transformer;
    }


    /**
     * @param inFilePath : path of the XML file to be transformed
     * @param inXslPath: path to the XSL file to be used for the transformation.
     * @return : a string that contains the transformed content
     */
    public static String transformXMLFileToHTML(final String inFilePath, final String inXslPath) {
        try {
            if (inFilePath == null || inFilePath.isEmpty()) {
                ReportConverter.LOGGER.error(REQUESTING_XSL_TRANSFORMATION_OF_IN_FILE_PATH_BEING_NULL_OR_EMPTY);
                return ReportConverter.ERROR_PERFORMING_XSL_TRANSFORMATION + REQUESTING_XSL_TRANSFORMATION_OF_IN_FILE_PATH_BEING_NULL_OR_EMPTY;
            }
            return transformXMLToHTML(new FileInputStream(new File(".").toURI().resolve(inFilePath).toURL().getFile()), inXslPath);
        } catch (final IOException e) {
            ReportConverter.LOGGER.error(e.getMessage());
            return "File IO Exception : " + inXslPath;
        }
    }
    public static String transformXMLToHTML(final InputStream xml, final String inXslPath) {
        try {
            final Transformer transformer = ReportConverter.prepareTransformation(inXslPath);
            if (transformer == null) {
                return ReportConverter.ERROR_PERFORMING_XSL_TRANSFORMATION + " unable to create transformer.";
            }
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            final StreamResult out = new StreamResult(baos);
            DocumentBuilder documentBuilder = new XmlDocumentBuilderFactory()
                    .setNamespaceAware(true).getBuilder();
            DOMSource source = null;
            try {
                source = new DOMSource(
                                documentBuilder.parse(
                                    new InputSource(
                                        new BOMInputStream(xml))));
            } catch (SAXException e) {
                ReportConverter.LOGGER.error(e.getMessage());
                return ReportConverter.ERROR_PERFORMING_XSL_TRANSFORMATION + e.getMessage();
            }
            transformer.transform(source, out);
            final String tmp = baos.toString(StandardCharsets.UTF_8.name());
            baos.close();
            return tmp;
        } catch (final IOException e) {
            ReportConverter.LOGGER.error(e.getMessage());
            return "File IO Exception : " + inXslPath;
        } catch (final TransformerException | DocumentBuilderFactoryException e) {
            ReportConverter.LOGGER.error(e.getMessage());
            return ReportConverter.ERROR_PERFORMING_XSL_TRANSFORMATION + e.getMessage();
        }
    }
    /**
     * Returns the XML string transformed using the given XSL
     *
     * @param inDetailedResult : the string representing the document to transform
     * @param inXslPath        : path to the XSLT file to use (relative to bin directory of project)
     * @return : a string that contains the transformed content
     */
    public static String resultTransformation(final String inDetailedResult, final String inXslPath, final String appNameValue) {

        try {
            final Transformer transformer = ReportConverter.prepareTransformation(inXslPath);
            if (transformer == null) {
                return ReportConverter.ERROR_PERFORMING_XSL_TRANSFORMATION + " unable to create transformer.";

            }
            transformer.setParameter("appName", appNameValue);
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            final StreamResult out = new StreamResult(baos);
            final ByteArrayInputStream bais = new ByteArrayInputStream(inDetailedResult.getBytes(StandardCharsets.UTF_8));
            transformer.transform(new StreamSource(bais), out);
            final String tmp = baos.toString(StandardCharsets.UTF_8.name());
            baos.close();
            return tmp;
        } catch (final IOException e) {
            ReportConverter.LOGGER.error(e.getMessage());
            return "File IO Exception : " + inXslPath;
        } catch (final TransformerException e) {
            ReportConverter.LOGGER.error(e.getMessage());
            return ReportConverter.ERROR_PERFORMING_XSL_TRANSFORMATION + e.getMessage();
        }
    }

    public static String transformXMLStringToHTML(final String documentContent, final String inXslCompletePath) throws InvalidStylesheetException {
        return transformXMLStringToHTML(documentContent, inXslCompletePath, "en");
    }


    public static String transformXMLStringToHTML(final String documentContent, final String inXslCompletePath, String language) throws InvalidStylesheetException {
        if (documentContent == null || documentContent.isEmpty()) {
            return null;
        }

        try {
            final String xslPath = inXslCompletePath;
            if (xslPath == null || xslPath.isEmpty()) {
                return null;
            }

            final Source xmlInput = new StreamSource(new StringReader(documentContent));
            final Transformer transformer = TransformerCache.instance().getTransformerFromCache(xslPath);
            if (transformer==null) {
                throw new InvalidStylesheetException(xslPath);
            }
        //    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, StandardCharsets.UTF_8.name());
            transformer.setOutputProperty(OutputKeys.METHOD, "html");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setParameter("ShowGeneralInformation", Boolean.FALSE);
            String assertionManagerUrl = PreferenceService.getString("assertion_manager_url");
            if (assertionManagerUrl != null && !assertionManagerUrl.endsWith("/")){
                assertionManagerUrl = assertionManagerUrl.concat("/");
            }
            transformer.setParameter("assertionManagerPath", assertionManagerUrl);
            transformer.setParameter("selectedLanguage" , language);
            transformer.setParameter("standalone" , Boolean.FALSE);
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            final StreamResult out = new StreamResult(baos);
            transformer.transform(xmlInput, out);
            final String tmp = baos.toString(StandardCharsets.UTF_8.name());
            baos.close();
            return tmp;

        } catch (TransformerException | UnsupportedEncodingException e) {
            ReportConverter.LOGGER.error(e.getMessage());
            return String.format("%s %s", ReportConverter.ERROR_PERFORMING_XSL_TRANSFORMATION, e.getMessage());
        } catch (final IOException e) {
            ReportConverter.LOGGER.error(e.getMessage());
            return String.format("File IO Exception : %s", e.getMessage());
        }
    }

    /**
     * @param inResponse : the string to be transformed.
     * @return : the transformed string.
     */
    @Deprecated
    public static String convertNistResponse(final String inResponse) {
        try {
            final Document nistResponse = XmlContentDecode.buildDocumentFromString(inResponse);
            final Node nistReportHeader;
            Node nistReportDetails = null;
            if (nistResponse == null) {
                return null;
            } else {
                nistReportHeader = nistResponse.getRootElement().selectSingleNode("ReportHeader");

                if (!"Incomplete".equals(nistReportHeader.selectSingleNode("ValidationStatus").getText())) {
                    nistReportDetails = nistResponse.getRootElement().selectSingleNode("ReportDetails")
                            .selectSingleNode("Hl7v2ValidationReport").selectSingleNode("MessageValidation")
                            .selectSingleNode("Result");
                }
            }

            final Element root = new Element("detailedResult");
            final org.jdom.Document convertResponse = new org.jdom.Document(root);

            final Element overview = new Element("ValidationResultsOverview");

            final Element date = new Element("ValidationDate");
            date.addContent(nistReportHeader.selectSingleNode("DateOfTest").getText());
            overview.addContent(date);

            final Element time = new Element("ValidationTime");
            time.addContent(nistReportHeader.selectSingleNode("TimeOfTest").getText());
            overview.addContent(time);

            final Element serviceVersion = new Element("ValidationServiceVersion");
            serviceVersion.addContent(nistReportHeader.selectSingleNode("ServiceVersion").getText());
            overview.addContent(serviceVersion);

            final Element engineVersion = new Element("ValidationEngineVersion");
            engineVersion.addContent(nistReportHeader.selectSingleNode("EngineVersion").getText());
            overview.addContent(engineVersion);

            final Element serviceName = new Element("ValidationServiceName");
            serviceName.addContent(nistReportHeader.selectSingleNode("ServiceName").getText());
            overview.addContent(serviceName);

            final Element result = new Element("ValidationTestResult");
            result.addContent(nistReportHeader.selectSingleNode("ResultOfTest").getText().toUpperCase(Locale.getDefault()));
            overview.addContent(result);

            root.addContent(overview);

            final Element counters = new Element("ValidationCounters");

            final Element errorCount = new Element("NrOfValidationErrors");
            errorCount.addContent(nistReportHeader.selectSingleNode("ErrorCount").getText());
            counters.addContent(errorCount);

            final Element warningCount = new Element("NrOfValidationWarnings");
            warningCount.addContent(nistReportHeader.selectSingleNode("WarningCount").getText());
            counters.addContent(warningCount);

            final Element noteCount = new Element("NrOfValidationConditions");
            noteCount.addContent(nistReportHeader.selectSingleNode("UserCount").getText());
            counters.addContent(noteCount);

            root.addContent(counters);

            final Element details = new Element("ValidationResults");

            final Element resultXml = new Element(ReportConverter.RESULT_XML);

            if (nistReportDetails == null) {
                final Element el = new Element(ReportConverter.ERROR);
                final Element description = new Element(ReportConverter.DESCRIPTION);
                description.addContent(nistReportHeader.selectSingleNode("ValidationStatusInfo").getText());
                el.addContent(description);
                el.addContent(new Element(ReportConverter.LOCATION));
                resultXml.addContent(el);
            } else {
                final List<?> errors = nistReportDetails.selectNodes(ReportConverter.ERROR);
                if (errors != null && !errors.isEmpty()) {
                    for (final Object error : errors) {
                        if (error instanceof Node) {
                            final Element el = new Element(ReportConverter.ERROR);
                            if (((Node) error).selectSingleNode(ReportConverter.DESCRIPTION) != null) {
                                final Element description = new Element(ReportConverter.DESCRIPTION);
                                description.addContent(((Node) error).selectSingleNode(ReportConverter.DESCRIPTION).getText());
                                el.addContent(description);
                            }
                            if (((Node) error).selectSingleNode(ReportConverter.LOCATION) != null) {
                                final Element location = new Element(ReportConverter.LOCATION);
                                final Node locNode = ((Node) error).selectSingleNode(ReportConverter.LOCATION);
                                final StringBuilder locationValue = new StringBuilder();

                                if (locNode.selectSingleNode("Segment") != null) {
                                    locationValue.append("Segment: ");
                                    locationValue.append(((org.dom4j.Element) locNode.selectSingleNode("Segment"))
                                            .attributeValue("Name"));
                                    locationValue.append('[')
                                            .append(((org.dom4j.Element) locNode.selectSingleNode("Segment"))
                                                    .attributeValue("InstanceNumber")).append("]\n");
                                }
                                if (locNode.selectSingleNode("Field") != null) {
                                    locationValue.append("Field: ");
                                    locationValue.append(((org.dom4j.Element) locNode.selectSingleNode("Field"))
                                            .attributeValue("Name"));
                                    locationValue.append('[')
                                            .append(((org.dom4j.Element) locNode.selectSingleNode("Field"))
                                                    .attributeValue("InstanceNumber")).append("]\n");
                                }
                                if (locNode.selectSingleNode("EPath") != null) {
                                    locationValue.append("EPath: ").append(locNode.selectSingleNode("EPath").getText())
                                            .append('\n');
                                }
                                if (locNode.selectSingleNode("Line") != null) {
                                    locationValue.append("line: ").append(locNode.selectSingleNode("Line").getText())
                                            .append('\n');
                                }
                                if (locNode.selectSingleNode("Column") != null) {
                                    locationValue.append("column: ")
                                            .append(locNode.selectSingleNode("Column").getText()).append('\n');
                                }
                                if (locNode.selectSingleNode("Component") != null) {
                                    locationValue.append("Component");
                                    locationValue.append(((org.dom4j.Element) locNode.selectSingleNode("Component"))
                                            .attributeValue("Name"));
                                }
                                location.addContent(locationValue.toString());
                                el.addContent(location);
                            }
                            resultXml.addContent(el);
                        }
                    }
                }

                final List<?> warnings = nistReportDetails.selectNodes(ReportConverter.WARNING);
                if (warnings != null && !warnings.isEmpty()) {
                    for (final Object warning : warnings) {
                        if (warning instanceof Node) {
                            final Element el = new Element(ReportConverter.WARNING);
                            if (((Node) warning).selectSingleNode(ReportConverter.DESCRIPTION) != null) {
                                final Element description = new Element(ReportConverter.DESCRIPTION);
                                description.addContent(((Node) warning).selectSingleNode(ReportConverter.DESCRIPTION).getText());
                                el.addContent(description);
                            }
                            if (((Node) warning).selectSingleNode(ReportConverter.LOCATION) != null) {
                                final Element location = new Element(ReportConverter.LOCATION);
                                final Node locNode = ((Node) warning).selectSingleNode(ReportConverter.LOCATION);
                                final StringBuilder locationValue = new StringBuilder();

                                if (locNode.selectSingleNode("Segment") != null) {
                                    locationValue.append("Segment: ");
                                    locationValue.append(((org.dom4j.Element) locNode.selectSingleNode("Segment"))
                                            .attributeValue("Name"));
                                    locationValue.append('[')
                                            .append(((org.dom4j.Element) locNode.selectSingleNode("Segment"))
                                                    .attributeValue("InstanceNumber")).append("]\n");
                                }
                                if (locNode.selectSingleNode("Field") != null) {
                                    locationValue.append("Field: ");
                                    locationValue.append(((org.dom4j.Element) locNode.selectSingleNode("Field"))
                                            .attributeValue("Name"));
                                    locationValue.append('[')
                                            .append(((org.dom4j.Element) locNode.selectSingleNode("Field"))
                                                    .attributeValue("InstanceNumber")).append("]\n");
                                }
                                if (locNode.selectSingleNode("EPath") != null) {
                                    locationValue.append("EPath: ").append(locNode.selectSingleNode("EPath").getText())
                                            .append('\n');
                                }
                                if (locNode.selectSingleNode("Line") != null) {
                                    locationValue.append("line: ").append(locNode.selectSingleNode("Line").getText())
                                            .append('\n');
                                }
                                if (locNode.selectSingleNode("Column") != null) {
                                    locationValue.append("column: ")
                                            .append(locNode.selectSingleNode("Column").getText()).append('\n');
                                }
                                if (locNode.selectSingleNode("Component") != null) {
                                    locationValue.append("Component: ");
                                    locationValue.append(((org.dom4j.Element) locNode.selectSingleNode("Component"))
                                            .attributeValue("Name"));
                                }
                                location.addContent(locationValue.toString());
                                el.addContent(location);
                            }
                            resultXml.addContent(el);
                        }
                    }
                }

                final List<?> notes = nistReportDetails.selectNodes("User");
                if (notes != null && !notes.isEmpty()) {
                    for (final Object note : notes) {
                        if (note instanceof Node) {
                            final Element el = new Element("Condition");
                            if (((Node) note).selectSingleNode(ReportConverter.DESCRIPTION) != null) {
                                final Element description = new Element(ReportConverter.DESCRIPTION);
                                description.addContent(((Node) note).selectSingleNode(ReportConverter.DESCRIPTION).getText());
                                el.addContent(description);
                            }
                            if (((Node) note).selectSingleNode(ReportConverter.LOCATION) != null) {
                                final Element location = new Element(ReportConverter.LOCATION);
                                final Node locNode = ((Node) note).selectSingleNode(ReportConverter.LOCATION);
                                final StringBuilder locationValue = new StringBuilder();

                                if (locNode.selectSingleNode("Segment") != null) {
                                    locationValue.append("Segment: ");
                                    locationValue.append(((org.dom4j.Element) locNode.selectSingleNode("Segment"))
                                            .attributeValue("Name"));
                                    locationValue.append('[')
                                            .append(((org.dom4j.Element) locNode.selectSingleNode("Segment"))
                                                    .attributeValue("InstanceNumber")).append("]\n");
                                }
                                if (locNode.selectSingleNode("Field") != null) {
                                    locationValue.append("Field: ");
                                    locationValue.append(((org.dom4j.Element) locNode.selectSingleNode("Field"))
                                            .attributeValue("Name"));
                                    locationValue.append('[')
                                            .append(((org.dom4j.Element) locNode.selectSingleNode("Field"))
                                                    .attributeValue("InstanceNumber")).append("]\n");
                                }
                                if (locNode.selectSingleNode("EPath") != null) {
                                    locationValue.append("EPath: ").append(locNode.selectSingleNode("EPath").getText())
                                            .append('\n');
                                }
                                if (locNode.selectSingleNode("Line") != null) {
                                    locationValue.append("line: ").append(locNode.selectSingleNode("Line").getText())
                                            .append('\n');
                                }
                                if (locNode.selectSingleNode("Column") != null) {
                                    locationValue.append("column: ")
                                            .append(locNode.selectSingleNode("Column").getText()).append('\n');
                                }
                                if (locNode.selectSingleNode("Component") != null) {
                                    locationValue.append("Component: ");
                                    locationValue.append(((org.dom4j.Element) locNode.selectSingleNode("Component"))
                                            .attributeValue("Name"));
                                }
                                location.addContent(locationValue.toString());
                                el.addContent(location);
                            }
                            resultXml.addContent(el);
                        }
                    }
                }
            }
            details.addContent(resultXml);
            root.addContent(details);
            final Format format = Format.getPrettyFormat();
            return new XMLOutputter(format).outputString(convertResponse);

        } catch (final RuntimeException e) {
            ReportConverter.LOGGER.error("an error occurred when converting NIST Response: {}", e.getMessage());
            return inResponse;
        }
    }

    /**
     * Use getUserIpAddress() from Gui bean. This method here is deprecated for incorrect responsability
     * @return
     */
    @Deprecated
    public static String getUserIpAddress() {
        return ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest())
                .getRemoteAddr();
    }

    /**
     * Parse the inString to find all strings referring to an URL and add the relevant hyperlink
     *
     * @param inString: the string that contains URL.
     * @return a string with the URL transformed as HTML hyperlink
     * @TODO must be finished
     */
    public static String makeHyperLink(final String inString) {
        if (inString == null || inString.isEmpty()) {
            return null;
        }
        //String regex = "((ftp|http|https|gopher|mailto|news|nntp|telnet|wais|file|prospero|aim|webcal):(([A-Za-z0-9$_.+!*(),;/?:@&~=-])|%[A-Fa-f0-9]{2}){2,}(#([a-zA-Z0-9][a-zA-Z0-9$_.+!*(),;/?:@&~=%-]*))?([A-Za-z0-9$_+!*();/?:~-]))";
        final String regex = "((ftp|http|https|file|mailto|telnet):(([A-Za-z0-9$_.+!*(),;/?:@&~=-])|%[A-Fa-f0-9]{2}){2,}(#([a-zA-Z0-9][a-zA-Z0-9$_.+!*(),;/?:@&~=%-]*))?([A-Za-z0-9$_+!*();/?:~-]))";
        return inString.replaceAll(regex, "<a href=\"$1\">$1</a>");
    }

    @Deprecated
    public static void exportToTxt(final String content, final String fileNameDestination) {
        ReportExporterManager.exportToTxt(content, fileNameDestination);
    }

    public static org.jdom.Document string2Document(final String xml) throws JDOMException, IOException {
        final SAXBuilder builder = new SAXBuilder();
        builder.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        builder.setFeature("http://xml.org/sax/features/external-general-entities", false);
        builder.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        final InputStream stream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
        return builder.build(stream);
    }

    public static boolean base64Decoding(final File fileToDecode, final File decodedFile) {

        byte[] decoded;

        try {
            String todecode = String.valueOf(Files.readAllLines(Paths.get(fileToDecode.getAbsolutePath()), StandardCharsets.UTF_8));
            decoded = DatatypeConverter.parseBase64Binary(todecode);
            FileUtils.writeByteArrayToFile(decodedFile, decoded);
            return true;
        } catch (final IOException e) {
            return false;
        }
    }
}
