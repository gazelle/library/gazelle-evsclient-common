package net.ihe.gazelle.evsclient.interlay.gui.document;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Iterators;
import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import net.ihe.gazelle.evsclient.interlay.gui.I18n;
import org.apache.commons.lang.StringUtils;
import org.richfaces.model.TreeNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class JsonTreeRenderer extends AbstractTreeRenderer<JsonTreeRenderer.JsonTreeNode,AbstractTreeRenderer.Options> {

    public static final String SPAN_CLOSING = "</span>";

    private static class KeyIterator implements Iterator<Object> {
        protected Iterator<?> children;
        public KeyIterator(final Iterator<?> delegate) {
            this.children = delegate;
        }
        @Override
        public boolean hasNext() {
            return children.hasNext();
        }
        @Override
        public Object next() {
            return children.next();
        }

        @Override
        public void remove() {
            children.remove();
        }
    }
    private static class IndexIterator extends KeyIterator {

        public IndexIterator(int size) {
            super(null);
            List<Integer> result = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                result.add(i);
            }
            this.children = result.iterator();
        }
        @Override
        public boolean hasNext() {
            return children.hasNext();
        }
        @Override
        public Object next() {
            return children.next();
        }

        @Override
        public void remove() {
            children.remove();
        }
    }
    public static class JsonTreeNode implements TreeNode {
        private String key;
        private JsonNode delegate;

        public JsonTreeNode() {
            super();
        }
        public JsonTreeNode(String key, JsonNode delegate) {
            super();
            this.key = key;
            this.delegate = delegate;
        }
        @Override
        public TreeNode getChild(Object o) {
            if (this.delegate.isArray()) {
                return new JsonTreeNode("["+o+"]",delegate.get((Integer)o));
            } else if (this.delegate.isContainerNode()) {
                return new JsonTreeNode(o.toString(), delegate.get(o.toString()));
            }
            return null;
        }

        @Override
        public int indexOf(Object o) {
            if (!this.isLeaf()) {
                Iterator<String> it = delegate.fieldNames();
                int i=0;
                while (it.hasNext()){
                    String name = it.next();
                    if (StringUtils.equals(name,o.toString())) {
                        return i;
                    }
                    i++;
                }
            }
            return -1;
        }

        @Override
        public Iterator<Object> getChildrenKeysIterator() {
            if (this.delegate.isArray()) {
                return new IndexIterator(this.delegate.size()) {
                };
            } else if (this.delegate.isContainerNode()) {
                return new KeyIterator(this.delegate.fieldNames());
            }
            return Iterators.emptyIterator();
        }

        @Override
        public boolean isLeaf() {
            return this.delegate!=null && !this.delegate.isContainerNode();
        }

        @Override
        public void addChild(Object o, TreeNode treeNode) {
            throw new IllegalStateException("Unmodifiable tree");
        }

        @Override
        public void insertChild(int i, Object o, TreeNode treeNode) {
            throw new IllegalStateException("Unmodifiable tree");
        }

        @Override
        public void removeChild(Object o) {
            throw new IllegalStateException("Unmodifiable tree");
        }
    }

    public JsonTreeRenderer() {
        super(new AbstractTreeRenderer.Options(),AbstractTreeRenderer.Options.class);
    }

    @Override
    public Boolean isActionAppropriate(String action, JsonTreeNode node) {
        return false;
    }

    @Override
    protected JsonTreeNode toTreeNode(HandledDocument doc) throws RenderingException {
        ObjectMapper om = new ObjectMapper();
        try {

            return toTreeNode(om.readTree(filter(doc.asJSONString())));
        } catch (IOException e) {
            throw new RenderingException(e);
        }
    }

    private JsonTreeNode toTreeNode(JsonNode jn) {
        return new JsonTreeNode("",jn);
    }

    private String filter(String jsonString){
        if(this.nodeFilter == null){
            return jsonString;
        }
        try{
            Object result = JsonPath.read(jsonString, this.nodeFilter);
            return "{\"result\":" + (new Gson()).toJson(result) + "}";
        }
        catch (PathNotFoundException e){
            String warningCaption = I18n.get("net.ihe.gazelle.evs.jsonPathNotFound") + ": " + this.nodeFilter;
            this.warningCaption = "</br></br><span class=\"rendering-error\"> "+ warningCaption + SPAN_CLOSING;
            return null;
        }
    }

    @Override
    public String getFilterLabel() {
        return I18n.get("net.ihe.gazelle.evs.jsonPathExpression");
    }

    @Override
    public String renderNode(JsonTreeNode node) {
        boolean isArrayIndex = node.key.matches("\\[\\d+\\]");
        StringBuilder sb = new StringBuilder("<span class=\"json-node json-")
                .append(node.delegate.getNodeType().toString().toLowerCase(Locale.ROOT))
                .append(isArrayIndex?" json-array-index":"")
                .append("\">")
                .append(node.key);
        if (node.isLeaf()) {
            sb.append(": <span class=\"json-value\">")
              .append(node.delegate.asText())
              .append(SPAN_CLOSING);
        } else {
            sb.append(" <span class=\"json-")
                    .append(node.delegate.getNodeType().toString().toLowerCase(Locale.ROOT))
                    .append("-marker\">")
            .append(node.delegate.isArray()?"[]":"{}")
            .append(SPAN_CLOSING);
        }
        return sb.append(SPAN_CLOSING).toString();
    }

    @Override
    public String renderNodeType(JsonTreeNode node) {
        return node.delegate.getNodeType().toString();
    }

    @Override
    protected JsonTreeNode emptyRendering() {
        return new JsonTreeNode();
    }

}
