package net.ihe.gazelle.evsclient.interlay.util;

public interface SyntaxDetector {
    Syntax detectSyntax(byte[] content);

    enum Syntax {

        XML("text/xml", ".xml"),
        JSON("text/json", ".json"),
        URL("text/url", ".url"),
        ZIP("application/zip", ".zip"),
        ER7("text/er7" , ".er7"),
        BIN("application/octet-stream", ".bin"),
        TXT("text/plain", ".txt"),
        HTML("text/html", ".html"),
        B64("text/base64", ".txt"),
        PDF("application/pdf", ".pdf"),
        DICOM("application/dicom", ".dcm"),
        PEM("application/x-pem-file", ".pem");


        private final String mimetype;

        private final String extension;

        Syntax(String mimetype, String extension) {
            this.mimetype = mimetype;
            this.extension = extension;
        }

        public String getMimetype() {
            return mimetype;
        }

        public String getExtension() {
            return extension;
        }
    }
}
