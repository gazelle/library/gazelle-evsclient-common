package net.ihe.gazelle.evsclient.interlay.gui.document;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class PemDocumentRenderer extends AbstractDocumentRenderer<AbstractDocumentRenderer.Options> {

    public PemDocumentRenderer() {
        super(new AbstractDocumentRenderer.Options(true),AbstractDocumentRenderer.Options.class);
        this.options.hasPrettyViewMode = true;
        this.options.prettyView = true;
    }

    @Override
    protected String prettify(HandledDocument doc) throws RenderingException {
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X509");
            X509Certificate cert = (X509Certificate) cf.generateCertificate(new ByteArrayInputStream(toByteArray(doc)));
            return cert.toString();
        } catch (Exception e) {
            throw new RenderingException(e);
        }
    }

    @Override
    protected String highlight(String message, Charset encoding) throws RenderingException {
        return message;
    }

    protected String toString(HandledDocument doc) throws RenderingException {
        // do not decode B64
        return doc.toString();
    }
}
