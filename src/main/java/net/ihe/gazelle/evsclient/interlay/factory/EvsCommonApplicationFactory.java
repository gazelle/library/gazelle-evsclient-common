package net.ihe.gazelle.evsclient.interlay.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.CallerMetadataFactory;
import net.ihe.gazelle.evsclient.application.OIDGeneratorManagerImpl;
import net.ihe.gazelle.evsclient.application.interfaces.ExportMapper;
import net.ihe.gazelle.evsclient.application.interfaces.OidGeneratorManager;
import net.ihe.gazelle.evsclient.interlay.util.CsvExportMapperImpl;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;

@Name("evsCommonApplicationFactory")
@Scope(ScopeType.APPLICATION)
@AutoCreate
public class EvsCommonApplicationFactory {

    @In(value = "evsCommonDaoFactory")
    private EvsCommonDaoFactory evsCommonDaoFactory;

    @In(value = "applicationPreferenceManager", create = true)
    private ApplicationPreferenceManager applicationPreferenceManager;

    @Factory( value = "oidGeneratorManager", scope = ScopeType.APPLICATION)
    public OidGeneratorManager getOidGeneratorManager(){
        return new OIDGeneratorManagerImpl(evsCommonDaoFactory.getOidSequenceDao(), applicationPreferenceManager);
    }

    @Factory( value = "callerMetadataFactory", scope = ScopeType.APPLICATION)
    public CallerMetadataFactory getCallerMetadataFactory(){
        return new CallerMetadataFactory();
    }

    @Factory( value = "csvExportMapper", scope = ScopeType.APPLICATION)
    public ExportMapper getCsvMapper() {
        return new CsvExportMapperImpl();
    }

}
