package net.ihe.gazelle.evsclient.interlay.util;

public class ExceptionUtil {

   private ExceptionUtil() {
   }

   public static String getMessageAndCauses(Throwable t) {
      StringBuilder sb = new StringBuilder();
      appendMessageAndCauses(sb, t);
      return sb.toString();
   }

   private static void appendMessageAndCauses(StringBuilder sb, Throwable t) {
      if (t != null) {
         sb.append(System.lineSeparator());
         sb.append("Caused by: ").append(t.getClass().getName());
         if(t.getMessage() != null) {
            sb.append(" ").append(t.getMessage());
         }
         appendMessageAndCauses(sb, t.getCause());
      }
   }

}
