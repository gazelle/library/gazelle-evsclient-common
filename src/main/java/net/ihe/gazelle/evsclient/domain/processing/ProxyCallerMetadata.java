package net.ihe.gazelle.evsclient.domain.processing;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "evsc_proxy_caller_metadata")
public class ProxyCallerMetadata extends OtherCallerMetadata {

    @Column(name = "proxy_type")
    private String proxyType;

    public ProxyCallerMetadata() {
    }

    public ProxyCallerMetadata(EntryPoint entryPoint, String toolOid, String toolObjectId, String proxyType) {
        super(entryPoint, toolOid, toolObjectId);
        this.proxyType = proxyType;
    }

    public String getProxyType() {
        return proxyType;
    }

    public void setProxyType(String proxyType) {
        this.proxyType = proxyType;
    }

    @Override
    public String toString() {
        return "ProxyCallerMetadata{" +
                "proxyType='" + proxyType + '\'' +
                '}';
    }
}
