package net.ihe.gazelle.evsclient.domain.processing;

public enum EntryPoint {
    GUI,
    WS;
}
