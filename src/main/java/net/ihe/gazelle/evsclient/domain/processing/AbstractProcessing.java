package net.ihe.gazelle.evsclient.domain.processing;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "evsc_processing")
@SequenceGenerator(name = "processing_sequence", sequenceName = "evsc_processing_id_seq", allocationSize = 1)
public abstract class AbstractProcessing implements Serializable {
    private static final long serialVersionUID = -3185896767365530657L;

    @Id
    @GeneratedValue(generator = "processing_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    protected Integer id;

    @Column(name = "oid")
    protected String oid;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "validation_date")
    protected Date date;

    @NotNull
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    protected List<HandledObject> objects;

    @OneToOne(cascade = CascadeType.ALL)
    protected OwnerMetadata owner;

    @NotNull
    @OneToOne(cascade = CascadeType.ALL)
    protected SharingMetadata sharing;

    @NotNull
    @OneToOne(cascade = CascadeType.ALL)
    protected EVSCallerMetadata caller;

    protected AbstractProcessing() {
        super();
        objects = new ArrayList<>();
    }

    protected AbstractProcessing(List<HandledObject> objects, EVSCallerMetadata caller) {
        this(objects, caller, null);
    }

    protected AbstractProcessing(List<HandledObject> objects, EVSCallerMetadata caller, OwnerMetadata ownerMetadata) {
        this.objects = objects;
        this.caller = caller;
        this.owner = ownerMetadata;
        this.sharing = new SharingMetadata();
        this.date = new Date();
    }

    public Date getDate() {
        return this.date != null ? (Date) this.date.clone() : null;
    }

    public void setDate(Date date){
        this.date = (Date) date.clone();
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public List<HandledObject> getObjects() {
        return objects;
    }

    public void setObjects(List<HandledObject> objects) {
        this.objects = objects != null ? new ArrayList<>(objects) : new ArrayList<HandledObject>();
    }

    public OwnerMetadata getOwner() {
        return owner;
    }

    public void setOwner(OwnerMetadata owner) {
        this.owner = owner;
    }

    public SharingMetadata getSharing() {
        return sharing;
    }

    public void setSharing(SharingMetadata sharing) {
        this.sharing = sharing;
    }

    public EVSCallerMetadata getCaller() {
        return caller;
    }

    public void setCaller(EVSCallerMetadata caller) {
        this.caller = caller;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPrivate(){
        this.sharing.setIsPrivate(Boolean.TRUE);
        this.sharing.setPrivacyKey(null);
    }

    public void setPublic(){
        this.sharing.setIsPrivate(Boolean.FALSE);
        this.sharing.setPrivacyKey(null);
    }

    public String getPrivacyKey() {
        return this.sharing.getPrivacyKey();
    }

    public void setPrivacyKey(String privacyKey) {
        this.sharing.setPrivacyKey(privacyKey);
    }

    public void setOwnerMetadata(String username, String organization) {
        this.owner.setUsername(username);
        this.owner.setOrganization(organization);
    }

    /**
     * @deprecated Since XValidation WS, Processing may be related to more than one validated object.
     * Many processing algorithms should be investigated this OneToMany relation impact against possible generalisation.
     */
    @Deprecated
    public HandledObject getObject() {
        return getObjects().iterator().next();
    }

    @Override
    public String toString() {
        return "AbstractProcessing{" +
                "id=" + id +
                ", oid='" + oid + '\'' +
                ", date=" + date +
                ", objects=" + objects +
                ", owner=" + owner +
                ", sharing=" + sharing +
                ", caller=" + caller +
                '}';
    }

}
