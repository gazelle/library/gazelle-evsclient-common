package net.ihe.gazelle.evsclient.domain.processing;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "evsc_caller_metadata")
@SequenceGenerator(name = "caller_metadata_sequence", sequenceName = "evsc_caller_metadata_id_seq", allocationSize = 1)
public class EVSCallerMetadata implements Serializable {

    private static final long serialVersionUID = 8349372728660093686L;

    @Id
    @GeneratedValue(generator = "caller_metadata_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private EntryPoint entryPoint;

    public EVSCallerMetadata() {
    }

    public EVSCallerMetadata(EntryPoint entryPoint) {
        this.entryPoint = entryPoint;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EntryPoint getEntryPoint() {
        return entryPoint;
    }

    public void setEntryPoint(EntryPoint entryPoint) {
        this.entryPoint = entryPoint;
    }

    @Override
    public String toString() {
        return "EVSCallerMetadata{" +
                "entryPoint=" + entryPoint +
                '}';
    }
}
