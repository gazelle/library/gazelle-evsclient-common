/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.ihe.gazelle.evsclient.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <b>Class Description :  </b>OIDGenerator<br><br>
 * This class describes the OIDGenerator object which is used to generate oid when the validation service
 * called does not respond or does not return an oid
 * <p/>
 * The root OID is defined in the ApplicationPreference table
 * <p/>
 * Attributes of this object are:
 * <ul>
 * <li><b>id</b> : id of the object in the database</li>
 * </ul>
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, June 16th
 * @class OIDGenerator
 * @package net.ihe.gazelle.evs.client.common.model
 * @see > Aberge@irisa.fr  -  http://www.ihe-europe.org
 */

@Entity
@Table(name = "evsc_oid", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "evsc_oid_sequence", sequenceName = "evsc_oid_id_seq", allocationSize = 1)
public class OIDSequence implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 831235001748497654L;

    @Id
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "evsc_oid_sequence")
    private Integer id;

    /**
     * Constructor
     */

    public OIDSequence() {

    }

    public OIDSequence(final Integer id) {
        this.id = id;
    }

    /**
     * Getter and Setter
     */
    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }
}
