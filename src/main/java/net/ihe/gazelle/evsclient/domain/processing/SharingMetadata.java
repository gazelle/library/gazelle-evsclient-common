package net.ihe.gazelle.evsclient.domain.processing;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "evsc_sharing_metadata")
@SequenceGenerator(name = "sharing_metadata_sequence", sequenceName = "evsc_sharing_metadata_id_seq", allocationSize = 1)
public class SharingMetadata implements Serializable {

    private static final long serialVersionUID = -4603405223911370056L;

    @Id
    @GeneratedValue(generator = "sharing_metadata_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    //FIXME change comment
    /**
     * this attribute is already set to true when the user is not logged when the user is logged, the default is false but the user is allowed to change it to make it public
     */
    @Column(name = "is_private")
    private Boolean isPrivate;

    /**
     * when isPrivate = true, the user can choose to share this validation result with some others. A privacy key is generated and will be provided in the permanent link as an authentication process
     */
    @Column(name = "privacy_key")
    private String privacyKey;

    public SharingMetadata() {
        super();
    }

    public Boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(Boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public String getPrivacyKey() {
        return privacyKey;
    }

    public void setPrivacyKey(String privacyKey) {
        this.privacyKey = privacyKey;
    }

    @Override
    public String toString() {
        return "SharingMetadata{" +
                "isPrivate=" + isPrivate +
                ", privacyKey='" + privacyKey + '\'' +
                '}';
    }
}
