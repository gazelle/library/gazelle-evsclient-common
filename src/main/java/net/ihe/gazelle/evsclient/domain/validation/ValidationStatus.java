package net.ihe.gazelle.evsclient.domain.validation;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "validationStatus")
@XmlEnum
public enum ValidationStatus {

    PENDING,
    IN_PROGRESS,
    DONE_PASSED,
    DONE_FAILED,
    DONE_UNDEFINED;

}
