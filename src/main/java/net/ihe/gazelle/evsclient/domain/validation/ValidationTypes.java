/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.domain.validation;

import gnu.trove.map.hash.THashMap;
import gnu.trove.set.hash.THashSet;
import org.reflections.Reflections;

import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public class ValidationTypes {
    public static String toCode(Class<? extends ValidationType> cls) {
        return cls.getSimpleName().replaceAll("(?<!\\w)([A-Z])","_$1").toUpperCase();
    }

    public static String toLabel(Class<? extends ValidationType> cls) {
        return cls.getSimpleName().replaceAll("(?<!\\w)([A-Z])"," $1");
    }

    private static Map<String, Class<? extends ValidationType>> types;
    public static Map<String, Class<? extends ValidationType>> collect() {
        Reflections reflections  = new Reflections("net.ihe.gazelle.evsclient.domain.validationservice.configuration.types");
        types = new THashMap<>();
        for (Class<? extends ValidationType> cls:reflections.getSubTypesOf(ValidationType.class)) {
            types.put(toLabel(cls),cls);
        }
        return types;
    }
    public static Set<Class<? extends ValidationType>> get() {
        if (types == null) {
            collect();
        }
        return new THashSet<>(types.values());
    }
    public static Set<Class<? extends ValidationType>> get(Class<ValidationType> filter) {
        Set<Class<? extends ValidationType>> selection = new THashSet<>();
        for (Class<? extends ValidationType> cls:get()) {
            if (filter.isAssignableFrom(cls)) {
                selection.add(cls);
            }
        }
        return selection;
    }
    public static Set<Class<? extends ValidationType>> get(String canonicalNamePattern) {
        Pattern p = Pattern.compile(canonicalNamePattern);
        Set<Class<? extends ValidationType>> selection = new THashSet<>();
        for (Class<? extends ValidationType> cls:get()) {
            if (p.matcher(cls.getCanonicalName()).find()) {
                selection.add(cls);
            }
        }
        return selection;
    }
}
