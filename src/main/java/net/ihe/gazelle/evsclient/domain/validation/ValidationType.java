/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.domain.validation;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/*
 * @deprecated ValidationType should disappear from future versions of Validation domain
 */
@XmlType(name = "ValidationType")
@XmlEnum
@Deprecated
public enum ValidationType {

    ATNA("ATNA"),
    AUDIT_MESSAGE("AuditMessage"),
    CDA("CDA"),
    DEFAULT("DEFAULT"),
    DICOM_WEB("DICOMWeb"),
    DICOM("DICOM"),
    DSUB("DSUB"),
    FHIR("FHIR"),
    HL7V2("HL7v2"),
    HL7V3("HL7v3"),
    HPD("HPD"),
    PDF("PDF"),
    SAML("SAML"),
    SVS("SVS"),
    TLS("TLS"),
    WS_TRUST("WSTrust"),
    XDS("XDS"),
    XDW("XDW"),
    XML("XML"),
    XVAL("CrossValidation");

    private final String value;

    ValidationType(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }

    public static boolean contains(final String test) {
        for (final ValidationType ft : ValidationType.values()) {
            if (ft.value.equals(test)) {
                return true;
            }
        }
        return false;
    }
}
