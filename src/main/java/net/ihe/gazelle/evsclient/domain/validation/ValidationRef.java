package net.ihe.gazelle.evsclient.domain.validation;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.io.*;
import java.nio.charset.StandardCharsets;

@Embeddable
public class ValidationRef implements Serializable {

    private static final long serialVersionUID = 1L;

    public static class Serialization {
        public static ValidationRef toValidationRef(byte[] ba) {
            String[] fields = new String(ba).split(",");
            return new ValidationRef(fields[0], fields[1]);
        }
        public static byte[] toByteArray(final ValidationRef value) {
            return new StringBuilder(value.getOid()).append(",").append(value.getValidationStatus()).toString().getBytes(StandardCharsets.UTF_8);
        }
        private Serialization() {}
    }

    @Column(name = "validation_oid")
    private String oid;

    @Transient
    private String validationStatus;

    public ValidationRef() {
    }

    public ValidationRef(String oid, String validationStatus) {
        this.oid = oid;
        this.validationStatus = validationStatus;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getValidationStatus() {
        return validationStatus;
    }

    public void setValidationStatus(String validationStatus) {
        this.validationStatus = validationStatus;
    }
}
