package net.ihe.gazelle.evsclient.domain.processing;

public class UnexpectedProcessingException extends Exception {

    public UnexpectedProcessingException(String message) {
        super(message);
    }

    public UnexpectedProcessingException(String message, Exception e) {
        super(message, e);
    }

    public UnexpectedProcessingException(Throwable throwable) {
        super(throwable);
    }
}
